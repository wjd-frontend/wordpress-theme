<?php
/**
 * The template for displaying the 404 template in the Twenty Twenty theme.
 *
 */

function wjd_404_body_class_search($classes) {
    $classes[] = 'search';
    return $classes;
}

add_filter('body_class', 'wjd_404_body_class_search');

get_header();
?>

<main id="site-content" role="main">
	<div class="custom-hero v-hero hero has-max-width mb-xxl" style="margin-bottom: 0px !important;">
		<div class="hero-swiper swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="display: none;">
					<div class="swiper-slide" style="height:500px !important;
									background-image:url('<?php echo THEME_PATH ?>/img/lost-place.jpg');
									background-position: center center;
									background-repeat: no-repeat;
									background-size:cover
									">
					</div>
					<div class="hero__box is-highlighted">
						<h1 class="hero__headline">404</h1>
						<h3 class="hero__subheadline"><?php _e( 'Die angeforderte Seite konnte nicht gefunden werden', 'wjd' ); ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="headline-section">
		<div class="headline-section__content container text-center">
			<h2 class="headline-section__headline is-highlighted"></h2>
		</div>
	</div>
	<div class="section" data-card-image-height="-1" data-card-image-orientation="image-middle-center">
		<div class="section__content container">
			<div class="wp-block-wjd-group">
				<div class="card has-no-image is-plain">
					<div class="card__content">
						<div class="card__main">
							<div class="rte-content v-margin-collapse">
								<p><?php _e( 'Schön, dass Sie den Weg zu den Wirtschaftsjunioren Deutschland gefunden haben! Wir sind rund 10.000 Unternehmer, Unternehmerinnen und Führungskräfte unter 40 Jahren. Wir sind global vernetzt und als größtes Netzwerk junger Wirtschaft in Deutschland mit rund 215 Mitgliedskreisen vor Ort präsent. Wer bei uns mitmacht, engagiert sich im Beruf und will darüber hinaus auch etwas bewegen.', 'wjd' ); ?></p>
								<p><?php _e( 'Diese Seite gibt es nicht mehr oder gab es noch nie, schauen Sie sich aber gern weiter bei uns um.', 'wjd' ); ?></p>
								<?php if (get_option('show_search') === 'on'): ?>
								<p><?php _e( 'Nutzen Sie gern die Suche um den angeforderten Inhalt zu finden!', 'wjd' ); ?></p>
								<?php endif; ?>
								<p><span style="color: #BEBEBE; font-size: 10px;">&copy; unsplash/Peter Herrmann</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php if (get_option('show_search') === 'on'): ?>
	<div class="section">
		<div class="section__content container">
			<div class="no-search-results-form section-inner thin">
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

</main>
<?php
get_footer();
