<?php
defined('ABSPATH') or die();
if (!function_exists('is_plugin_active')) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
} ?>
<?php get_header(); ?>
<?php if ( is_front_page() && is_home() ) {
    $pageTemplate = TEMPLATEPATH . '/templates/default_home.php';
} elseif ( is_front_page() ) {
    $pageTemplate = TEMPLATEPATH . '/templates/static_home.php';
} elseif ( is_home() ) {
    $pageTemplate = TEMPLATEPATH . '/templates/static_blog.php';
} elseif ( is_category() ) {
    $pageTemplate = TEMPLATEPATH . '/templates/static_category.php';
} elseif ( is_archive() && get_post_type() === 'magazines' && class_exists('WjdMagazin')) {
    if (is_tax('issues')) {
        $pageTemplate = WJD_MAGAZIN_PLUGIN_PATH . '/templates/magazin_issue_archive.php';
    }
    if (is_tax('magazine-tags')) {
        $pageTemplate = WJD_MAGAZIN_PLUGIN_PATH . '/templates/magazin_tag_archive.php';
    }
} elseif ( is_archive() ) {
    $pageTemplate = TEMPLATEPATH . '/templates/static_archive.php';
} elseif ( is_search() ) {
    $pageTemplate = TEMPLATEPATH . '/templates/searchpage.php';
} else {
    $pageTemplate = TEMPLATEPATH . '/templates/static_index.php';
}
require_once $pageTemplate;
?>
