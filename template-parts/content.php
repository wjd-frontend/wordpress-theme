<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
    <?php if (has_post_thumbnail()): ?>
        <div class="thumbnail">
            <?php the_post_thumbnail()  ?>
        </div>
    <?php endif; ?>
    <div class="entry-content">
        <h4><a href="<?= get_permalink() ?>" target="_blank"><?= get_the_title() ?></a></h4>
        <p><?= get_the_excerpt(); ?></p>
        <?php
            $viewLink = removeQueryParameter(get_permalink(), '_rt');
            $viewLink = removeQueryParameter($viewLink, '_rt_nonce');
        ?>
        <a class="more-link" href="<?= get_permalink() ?>" target="_blank"><?= $viewLink ?></a>
    </div>
</article>

