<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
    <?php if (has_post_thumbnail()): ?>
        <div class="thumbnail placeholder"></div>
    <?php endif; ?>
    <div class="entry-content">
        <h4><a href="<?= get_permalink() ?>" target="_blank">Intern</a></h4>
        <p>Bitte melden Sie sich an um diese Inhalte zu sehen.</p>
        <a class="more-link" href="<?= get_permalink() ?>" target="_blank"><?= get_permalink() ?></a>
    </div>
</article>
