<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
    <?php if (wp_attachment_is_image()): ?>
        <a href="<?= wp_get_attachment_url() ?>" target="_blank">
            <img class="thumbnail" src="<?= wp_get_attachment_url() ?>">
            <div class="entry-content">
                <h4><?= get_the_title() ?></h4>
                <p class="filesize"><?= byteToReadableFilter(filesize( get_attached_file( get_the_ID() ) ));?></p>
            </div>
        </a>
    <?php else: ?>
        <a href="<?= wp_get_attachment_url() ?>" target="_blank">
            <div class="file-placeholder"></div>
            <div class="entry-content">
                <h4><?= get_the_title() ?></h4>
                <p class="filesize"><?= byteToReadableFilter(filesize( get_attached_file( get_the_ID() ) ));?></p>
            </div>
        </a>
    <?php endif; ?>
</article>