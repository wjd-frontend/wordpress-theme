<?php

namespace WJD\Utility;

defined('ABSPATH') or die;

class PageMetaBox
{
    public function __construct()
    {
        add_action('add_meta_boxes', [$this, 'add_custom_meta_box']);

        add_action('save_post', [$this, 'save_custom_meta_box'], 10, 2);
    }

    public function add_custom_meta_box()
    {
        add_meta_box(
            'wjd_anchor_menu-meta-box',
            __('Ankermenü', 'wjd'),
            [$this, 'wjd_anchor_menu_box_markup'],
            'page',
            'side',
            'high',
            null
        );
    }
    public function wjd_anchor_menu_box_markup($post)
    {
        wp_nonce_field(basename(__FILE__), "checkbox_nonce");
        $checkbox_stored_meta = get_post_meta($post->ID); ?>
        <p><?php _e('Generiert automatisch ein sticky Ankermenü an der Seite, basierend auf den gewählten Sektionen', 'wjd')?></p>
        <label for="_show_anchor_menu">
            <input type="checkbox" name="_show_anchor_menu" id="_show_anchor_menu" value="yes" <?php if (isset($checkbox_stored_meta['_show_anchor_menu'])) {
                checked($checkbox_stored_meta['_show_anchor_menu'][0], 'yes');
            }
            ?> />
          <?php _e('Anzeigen?', 'wjd')?>
        </label>

        <?php

    }

    public function save_custom_meta_box($post_id)
    {
        $is_autosave = wp_is_post_autosave($post_id);
        $is_revision = wp_is_post_revision($post_id);
        $is_valid_nonce = (isset($_POST['checkbox_nonce']) && wp_verify_nonce($_POST['checkbox_nonce'], basename(__FILE__))) ? 'true' : 'false';

        if ($is_autosave || $is_revision || !$is_valid_nonce) {
            return;
        }
        
        if (isset($_POST['_show_anchor_menu'])) {
            update_post_meta($post_id, '_show_anchor_menu', 'yes');
        } else {
            update_post_meta($post_id, '_show_anchor_menu', '');
        }
    }
}
