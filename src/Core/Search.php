<?php 
namespace WJD\Core;

defined('ABSPATH') or die();

class Search 
{
    public function __construct()
    {
        add_action('admin_init', [$this, 'general_search_section']);
    }

    public function general_search_section()
    {  
        add_settings_section(  
            'search_settings_section',
            'Optionen für globale Suche',
            __return_empty_string(),
            'general'
        ); 

        add_settings_field(
            'show_search',
            'Globale Suche anzeigen',
            [$this, 'search_toggle_callback'],
            'general',
            'search_settings_section',
            array(
                'show_search'
            )  
        );

        register_setting('general', 'show_search', 'esc_attr');       
    }

    public function search_toggle_callback($args) 
    {
        $option = get_option($args[0]);
        ob_start(); ?>
            <input type="checkbox" id="<?= $args[0] ?>" name="<?= $args[0] ?>" <?php checked( $option, 'on' ) ?>/>
        <?= ob_get_clean();
    }
    
}