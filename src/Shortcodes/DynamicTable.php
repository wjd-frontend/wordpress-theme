<?php

namespace WJD\Shortcodes;

defined('ABSPATH') or die();

class DynamicTable
{
    public function __construct()
    {
        add_shortcode('wjd_dynamic_table', [$this, 'wjd_member_table_shortcode']);
    }

    public function wjd_member_table_shortcode($atts)
    {
        wp_enqueue_script(THEME_NAME.'-datatables', THEME_PATH . '/js/datatables/jquery.dataTables.js', [], ASSET_VERSION, true);
        wp_enqueue_script(THEME_NAME.'-datatables-bt4', THEME_PATH . '/js/datatables/dataTables.bootstrap4.min.js', [], ASSET_VERSION, true);
        wp_enqueue_script(THEME_NAME.'-datatables-resp', THEME_PATH . '/js/datatables/dataTables.responsive.min.js', [], ASSET_VERSION, true);
        wp_enqueue_script(THEME_NAME.'-datatables-resp-bt4', THEME_PATH . '/js/datatables/responsive.bootstrap4.min.js', [], ASSET_VERSION, true);

        wp_enqueue_style(THEME_NAME . '-datable-style', THEME_PATH . '/css/dataTables.bootstrap4.min.css');
        wp_enqueue_style(THEME_NAME . '-datable--resp-style', THEME_PATH . '/css/responsive.bootstrap4.min.css');

        $atts = shortcode_atts([
            'src' => '',
            'columns' => ''
        ], $atts);

        if (strlen($atts['src']) <= 0) {
            return '<p>Bitte geben Sie eine Datenquelle für die Tabelle an.</p>';
        }        
        if (strlen($atts['columns']) <= 0) {
            return '<p>Bitte geben Sie die Spalten für die Tabelle an. Im Format columns="{Titel_1,feldname_1}{Titel_2,feldname_2}..."</p>';
        }        
        ob_start(); ?>
            <table class="datatable-holder table table-striped table-bordered dt-responsive nowrap" width="100%" data-wjdsrc="<?= $atts['src'] ?>" data-wjdcolumns="<?= $atts['columns'] ?>"></table>
        <?php return ob_get_clean();
    }
}