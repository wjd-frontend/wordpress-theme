# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.17] - 2025-03-06
### Added
- New Styles for Form Creator Forms

## [1.3.16] - 2024-09-26
### Fixed
- Fixed Datatables breaking screen width with very long contents
- Fixed event details maybe hidden if detail page is not detected correctly

## [1.3.15] - 2024-07-25
### Fixed
- Fixed Gravity Forms Breaking on adding Styling Classes 

## [1.3.14] - 2024-06-10
### Fixed
- Fixed Import/Export Buttons in Calendar Archive

## [1.3.13] - 2024-06-10
### Fixed
- Fix for Calendar Archive Page

## [1.3.12] - 2023-12-22
### Fixed
- Fix for Archive Page Excerpt Generation

## [1.3.11] - 2023-10-27
### Fixed
- Minor Code Cleanup

## [1.3.10] - 2023-10-27
### Fixed
- Added new Styles for JSON generated Forms

## [1.3.9] - 2023-09-12
### Fixed
- Content of Events being broken by default Gutenberg Content getting added

## [1.3.8] - 2023-05-26
### Fixed
- Fixed Search Styles on Safari and Mobile Devices

## [1.3.7] - 2023-05-06
### Fixed
- Fixed Button in Header in Meta Nav loosing it's Padding if it's the only item

## [1.3.6] - 2023-04-14
### Fixed
- Fixed Quotes displaying a broken Image if the Image is wider then higher
### Added
- Added Cards in Sections with set same Height to be have the same height in a Row

## [1.3.5] - 2023-03-31
### Fixed
- Anchor Menu Active Element now displays current Menu Item correctly, ignoring empty section headlines

## [1.3.4] - 2023-02-28
### Added
- Another Extension for Ajax Archives using the load More Button

## [1.3.3] - 2023-02-27
### Fixed
- Fixed a possible Error in Height Calculation of Masonry Archives
### Added
- Small Extension for Ajax Archives using the load More Button

## [1.3.2] - 2023-01-23
### Fixed
- Fixed potentail Duplication of Nav-Markup in Calendar View
### Added
- New Per-Page Selectable Off-Canvas Anchormenu

## [1.3.1] - 2023-01-06
### Fixed
- Fix Links on responsive Menu Level 3 copying over their Subtitles for Level 2 Parent Items

## [1.2.9] - 2023-01-06
### Added
- Added new Datatables

## [1.2.8] - 2022-11-25
### Fixed
- Fixed Alignment of Cards in Columns to Cards not in Colums

## [1.2.7] - 2022-11-21
### Added
- Added Styles for new Parallax Section

## [1.2.6] - 2022-11-04
### Fixed
- Calendar Styles for MyCalender Plugin updated

## [1.2.5] - 2022-10-26
### Added
- Added new styles for new GB Blocks (Hero with CTA, Postlist)

## [1.2.4] - 2022-10-26
### Fixed
- Fixed Pagination in Category Archives

## [1.2.4] - 2022-10-07
### Added
- Enabled Revisions for Projects and Positions

## [1.2.3] - 2022-09-16
### Added
- Updated Search Page for Logged In Users and internal pages/media

## [1.2.2] - 2022-09-15
### Added
- issue #103 - anchor menu for sections
- issue #95 - search adjustments

### Fixed
- issue #106 - card vertical styles
- issue #107 - card vertical no image rendering

## [1.2.1] - 2022-09-14
### Added
- issue #85 - documentation of latest_news shortcode
- issue #99 - nice looking 404 page
- issue #86 - added borlabs cookie documentation to readme.md

### Fixed
- issue #108 - wpautop in dynamic_sidebar causes ps, removed in header.php
- issue #105 - error not showing search when not having a logo set
- issue #92 - code quality issue concating non existing var

## [1.2.0] - 2022-09-13
### Added
- issue #95 - implementation of search functionality
- issue #103 - implementation of anchor menu

### Fixed 
- issue #98 - minor fixes for youtube cors problem
- optical fixes in cards

## [1.1.18] - 2022-11-03
### Fixed
- REQUIRED FOR WORDPRESS v6.1
- Hotfix for Menu Items not opening

## [1.1.17] - 2022-08-12
### Changed
- removed nova code breaking borlabs

## [1.1.16] - 2022-08-08
### Changed
- replaced taxonomies for projekte and positionen for hierarchy

## [1.1.15] - 2022-08-05
### Fixed
- fixed JS Mapping Files for Bootstrap & Swiper

## [1.1.14] - 2022-07-25
### Changed
- added taxonomies for projekte and positionen
- cleaned up NOVA PHP Code

## [1.1.13] - 2022-07-25
### Changed
- made positionen also publicly queryable like projekte
- enabled archives for projekte and positionen

## [1.1.12] - 2022-06-23
### Added

### Changed

### Fixed
- issue #89 - Added Workaround for Pagination breaking on category archive pages

## [1.1.11] - 2022-06-23
### Added

### Changed

### Fixed
- latest_news Shortcode broken on posts without post images

## [1.1.10] - 2022-06-14
### Added

### Changed

### Fixed
- issue #88 - Plaintext Karte mit Bild links/rechtsbündig

## [1.1.9] - 2022-06-09
### Added

### Changed

### Fixed
- issue #87 - Darstellungsfehler bei News (nach WP Update)
- issue #83 - Videoeinbindung über URL (YouTube) funktioniert nicht
- issue #99 - Autoplay bei mp4 Einbettung in Video Element

## [1.1.6] - 2022-04-27
### Added
- issue #66: custom copyright for media
- issue #77: added CHANGELOG.md (this file :-))
- issue #58: added compatibility for instagram and linkedin

### Changed

### Fixed
