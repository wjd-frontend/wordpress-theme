<?php
defined('ABSPATH') or die();
?>
<?php get_header(); ?>
<main>
    <?php the_post(); ?>
    <?php the_content(); ?>
    <?php if (get_post_meta(get_the_id(), '_show_anchor_menu', true)) {        
        get_template_part( 'template-parts/anchor', 'menu' );
    } ?>
</main>
<?php
get_footer(); 
