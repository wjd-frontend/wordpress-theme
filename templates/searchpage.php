<?php get_header(); ?>

<main id="site-content" role="main">
    <div class="section">
        <div class="section__content container">
            <?php if (is_search()): global $wp_query; ?>
                <div class="v-hero hero has-max-width">
                    <div class="hero__box is-highlighted">
                        <h1 class="hero__headline">Suche: „<?= get_search_query() ?>“</h1>
                    </div>
                </div>
                <?php if (strlen(get_search_query()) < 3): ?>
                    <h3 class="sub-headline">Ihr Suchbegriff muss mindestens drei Zeichen enthalten.</h3>
                <?php else: ?>
                    <?php if ( $wp_query->found_posts ): ?>
                        <h3 class="sub-headline">Es gibt <?= number_format_i18n( $wp_query->found_posts ) ?> Treffer für Ihre Suche.</h3>
                    <?php else: ?>
                        <h3 class="sub-headline">Wir konnten keine Ergebnisse für Ihre Suche finden. Sie können es noch einmal über das Suchformular unten versuchen.</h3>
                    <?php endif;
                endif;
            endif; ?>
            <div class="no-search-results-form section-inner thin">
                <?php get_search_form(); ?>
            </div>

            <?php if (strlen(get_search_query()) >= 3 && have_posts()):
                while (have_posts()):
                    the_post();
                    if (get_transient( 'wjd_cog_login' )) {
                        get_template_part( 'template-parts/content', get_post_type() );
                        continue;
                    }
                    if (get_post_meta(get_the_id(), '_is_wjd_internal', true) === 'yes' || get_post_meta(get_the_id(), '_wjd_protected', true) === '1') { // TODO: check logged in and echo dependent
                        get_template_part( 'template-parts/content', 'internal-'.get_post_type() );
                    } else {
                        get_template_part( 'template-parts/content', get_post_type() );
                    }
                endwhile; ?>
                <?php $posts_pagination = get_the_posts_pagination([    
                    'mid_size'  => 2,
                    'prev_text' => '',
                    'next_text' => '',
                ]);
                if ($posts_pagination): ?>
                    <div class="pagination-wrapper">
                        <?php echo $posts_pagination; ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
    <?php if (get_post_meta(get_the_id(), '_show_anchor_menu', true)) {        
        get_template_part( 'template-parts/anchor', 'menu' );
    } ?>
</main>
<?php get_footer();
