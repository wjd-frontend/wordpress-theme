<?php

defined('ABSPATH') or die();

/**
 * WJD2020 Wordpress Theme Functions
 */
$wjd_theme = wp_get_theme();
define('ASSET_VERSION', $wjd_theme->get('Version'));
define('THEME_NAME', $wjd_theme->get('TextDomain'));
define('THEME_PATH', get_template_directory_uri()); 

require_once('vendor/autoload.php');

require_once('demo-import/demo-import.php');

$calendar = new WJD\Calendar\CalendarExtension();

// Default content for new page
add_filter( 'default_content', 'default_content_page_post', 10, 2 );
function default_content_page_post($default_content, $post) 
{
    if (in_array($post->post_type, ['post', 'page', 'positionen', 'projekte', 'magazines'])) {
        $default_content = '<!-- wp:wjd/hero -->
        <div class="wp-block-wjd-hero"><div class="gallery-grid" data-total-slides="0"></div></div>
        <!-- /wp:wjd/hero -->
    
        <!-- wp:wjd/group -->
        <div class="wp-block-wjd-group"></div>
        <!-- /wp:wjd/group -->';
    }
    return $default_content;
}

/** 
 * Custom Copyright for Media
 * inspired by https://bavotasan.com/2012/add-a-copyright-field-to-the-media-uploader-in-wordpress/
 */

/**
 * Adding a "Copyright" field to the media uploader $form_fields array
 *
 * @param array $form_fields
 * @param object $post
 *
 * @return array
 */
function add_copyright_field_to_media_uploader( $form_fields, $post ) {
	$form_fields['copyright_field'] = array(
		'label' => __('Copyright'),
		'value' => get_post_meta( $post->ID, '_custom_copyright', true ),
		'helps' => __('Urheberrechtsangabe für das Bild')
	);
	
	if (is_plugin_active('wordpress-plugin-connect/wordpress-plugin-connect.php')) {
		$wjd_protected = get_post_meta($post->ID, '_wjd_protected', true);
		$form_fields['wjd_protected'] = array(
		    'label' => __('Intern geschützt'),
    		'input' => 'html',
    		'html' => '<label for="attachments-'.$post->ID.'-wjd_protected"><input type="checkbox" id="attachments-'.$post->ID.'-wjd_protected" name="attachments['.$post->ID.'][wjd_protected]" value="1"'.($wjd_protected ? ' checked="checked"' : '').' /> geschützt</label>',
    		'value' => $wjd_protected,
    		'helps' => __('Wird u.a. nicht in Suche ausgegeben.')
    	);	
	}

	return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'add_copyright_field_to_media_uploader', null, 2 );

/**
 * Save our new "Copyright" field
 *
 * @param object $post
 * @param object $attachment
 *
 * @return array
 */
function add_copyright_field_to_media_uploader_save( $post, $attachment ) {
	if ( ! empty( $attachment['copyright_field'] ) ) 
		update_post_meta( $post['ID'], '_custom_copyright', $attachment['copyright_field'] );
	else
		delete_post_meta( $post['ID'], '_custom_copyright' );
	
	if ( ! empty( $attachment['wjd_protected'] ) ) 
		update_post_meta( $post['ID'], '_wjd_protected', $attachment['wjd_protected'] );
	else
		delete_post_meta( $post['ID'], '_wjd_protected' );

	return $post;
}
add_filter( 'attachment_fields_to_save', 'add_copyright_field_to_media_uploader_save', null, 2 );

/**
 * Display our new "Copyright" field
 *
 * @param int $attachment_id
 *
 * @return array
 */
function get_media_copyright( $attachment_id = null ) {
	if ( $attachment_id )
		return get_post_meta( $attachment_id, '_custom_copyright', true );

}

/*
 ** Altered default CF7 Shortcodes
 */
add_action('after_setup_theme','wjd_wpcf7_submit_button');
function wjd_wpcf7_submit_button() 
{
    if(function_exists('wpcf7_remove_form_tag')) {
        wpcf7_remove_form_tag('submit');
        remove_action( 'admin_init', 'wpcf7_add_tag_generator_submit', 55 );
        $wjd_cf7_module = TEMPLATEPATH . '/cf7/submit.php';
        require_once $wjd_cf7_module;
    }
}

add_action('after_setup_theme','wjd_wpcf7_acceptance');
function wjd_wpcf7_acceptance() 
{
    if(function_exists('wpcf7_remove_form_tag')) {
        wpcf7_remove_form_tag('acceptance');
        remove_action( 'admin_init', 'wpcf7_add_tag_generator_acceptance', 55 );
        $wjd_cf7_module = TEMPLATEPATH . '/cf7/acceptance.php';
        require_once $wjd_cf7_module;
    }
}

add_action('after_setup_theme','wjd_wpcf7_checkbox');
function wjd_wpcf7_checkbox() 
{
if(function_exists('wpcf7_remove_form_tag')) {
        wpcf7_remove_form_tag('radio');
        remove_action( 'admin_init', 'wpcf7_add_tag_generator_checkbox_and_radio', 55 );
        $wjd_cf7_module = TEMPLATEPATH . '/cf7/checkbox.php';
        require_once $wjd_cf7_module;
    }
}

add_action('admin_enqueue_scripts', 'wjd_nova_admin_script');

function wjd_nova_admin_script()
{
    wp_register_script('wjd-nova-backend-option', THEME_PATH.'/js/backend-option.js', array('jquery'), '1.0.0', ASSET_VERSION);
    wp_enqueue_script('wjd-nova-backend-option');
}

add_action( 'admin_enqueue_scripts', 'nova_admin_styles' );
function nova_admin_styles() {
    wp_enqueue_style( 'admin_css_nova', get_template_directory_uri() . '/scss/vendor/guide-block-lines.css', false, '1.0.0' );
    wp_enqueue_style( 'admin_css_nova_2', get_template_directory_uri() . '/scss/admin/block-editor.css', false, '1.0.0' );
}  

$wjd = new WJD\Core\WJD();

$gformFilters = new \WJD\Utility\GformFilter();

$search = new \WJD\Core\Search();

function byteToReadableFilter($bytes)
{      
    $bytes = (int)$bytes;  
    $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;

    if ($bytes < $kilobyte) {
        $output = $bytes . ' B';
    } elseif ($bytes < $megabyte) {
        $output = number_format(($bytes / $kilobyte), 2, ',', '.') . ' KiB';
    } elseif ($bytes < $gigabyte) {
        $output = number_format(($bytes / $megabyte), 2, ',', '.' ) . ' MiB';
    } elseif ($bytes < $terabyte) {
        $output = number_format(($bytes / $gigabyte), 2, ',', '.' ) . ' GiB';
    } else {
        $output = number_format(($bytes / $terabyte), 2, ',', '.' ) . ' TiB';
    }
    return $output;
}

add_filter( 'relevanssi_indexing_restriction', 'rlv_no_image_attachments' );
function rlv_no_image_attachments( $restriction ) 
{
    global $wpdb;
    $restriction['mysql']  .= " AND post.ID NOT IN (SELECT ID FROM $wpdb->posts WHERE post_type = 'attachment' AND post_mime_type LIKE 'image%' ) ";
    $restriction['reason'] .= ' No images';
    return $restriction;
}

function removeQueryParameter($url, $param) 
{
    if (mb_strpos($url, '?') === false) {
        return $url;
    }
    list($baseUrl, $urlQuery) = explode('?', $url, 2);
    parse_str($urlQuery, $urlQueryArr);
    unset($urlQueryArr[$param]);

    if(count($urlQueryArr))  {
        return $baseUrl.'?'.http_build_query($urlQueryArr);
    } else {
        return $baseUrl;
    }
}