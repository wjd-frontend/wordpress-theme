# CSS and JavaScript Build System
Das Build System generiert aus den scss Dateien die style.css sowie eine komprimierte Fassung des Javascripts.
Bei Änderungen in den Scripten oder Styles ist das Build System vor einem Commit zu nutzen um eine Konsistenz der Daten zu gewährleisten.

## Voraussetzungen
Das Build-System benötigt eine node.js Umgebung: https://nodejs.org/en/download/

## Build System installieren
Hierzu im Ordner **build** folgenden Befehl ausführen `npm install`

## Build System konfigurieren
Hierzu im Ordner **build** den Befehl `setup_gulp.bat` ausführen

## Build System nutzen
1. Styles:
`gulp styles`

2. Scripts
`gulp scripts`
