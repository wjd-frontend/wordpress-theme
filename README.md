# Wordpress Theme Wirtschaftsjunioren Deutschland e.V.

# Voraussetzungen
Um das Theme hochladen und installieren zu können sowie die Demo Daten zu importieren, ist es nötig, dass der Server bestimmte PHP-Einstellungen korrekt gesetzt hat.

Folgende Werte sind als Minimums empfohlen:

- memory_limit = 200M
- max_execution_time = 60
- post_max_size = 16M
- upload_max_filesize = 16M

# Installation
## Theme importieren
![Screenshot Theme Installation](/doc/theme-upload.png "Theme Installation")

Im Menüpunkt “Design” auf den Unterpunkt “Themes” klicken und dort auf den Button “Theme hinzufügen klicken”. Im neuen Fenster dann an derselben Position auf den Button “Theme hochladen” klicken und dann über “Choose File” die Theme-Datei auswählen und anschließend auf “Jetzt installieren” klicken.

## Child-Theme
Sofern ihr plant, dass Theme zu erweitern oder anzupassen, nutzt dafür das vorhandene Child-Theme. Damit bleiben Änderungen auch bei Updates am Haupt-Theme in eurer Instanz wirksam!

## Theme aktivieren
![Screenshot Theme aktivieren](/doc/theme-activate.png "Theme aktivieren")

Im Menüpunkt “Design” auf den Unterpunkt “Themes” klicken und dort das neu hinzugefügte Theme “WJD-Theme” aktivieren. 

## Plugins installieren
![Screenshot Plugins installieren](/doc/theme-plugin-installer-1.png "Plugins installieren")

Sobald das Theme aktiviert wurde erscheint oben ein neues Fenster, das über die benötigten Plugins informiert. Dort auf “Begin installing Plugins” klicken.

![Screenshot Plugins installieren](/doc/theme-plugin-installer-2.png "Plugins installieren")

Im neuen Fenster dann alle Häckchen ausfüllen und als Aktion “Install” auswählen. Beim Klick auf “Übernehmen” werden die Plugins dann installiert.

## Demo-Daten importieren
Falls noch nicht geschehen, einen beliebigen Menüpunkt auswählen, um das Menü mit den neuen Optionen der Plugins zu aktualisieren.
Anschließend beim Punkt “Design” auf den Unterpunkt “Import Demo Data” klicken und im neuen Fenster auf den “Import Demo Data” Button klicken.
Es sollten jetzt die Demo-Daten installiert werden und nach dem Abschluss bei den Seiten, Formularen, Projekten etc. sichtbar sein.

![Screenshot Demo-Daten importieren](/doc/import-demo-data.png "Demo-Daten importieren")

# Grundkonfiguration
Die Demo-Daten bieten eine solide Grundlage um mit dem Theme zu arbeiten. Im Folgenden gehen wir auf einige Aspekte der Grundkonfiguration ein.

## Seitentitel 
Der Titel der Website kann im Menü unter **Einstellungen** und dann beim Unterpunkt **Allgemein** festgelegt werden.
Alternativ auch über den **Customizer** unter dem Menüpunkt **Website-Informationen** (siehe nächster Abschnitt).

## Customizer (Logo / Favicon)
Auch Website-Logo (Bild im Header) und das Website-Icon (Bild zu sehen im Browser-Tab), auch Favicon genannt, lassen sich über den **Customizer** anpassen.
Dieser muss zunächst aufgerufen bzw. gestartet werden.

Dazu wählt man im Menü den Punkt **Design** und anschließend den Unterpunkt **Customizer**.
Im Customizer kann man neben Logo und Icon auch noch andere Einstellungen treffen, die aber sonst auch über das normale Dashboard bearbeitet werden können.

Unter dem Customizer-Menüpunkt **Website-Informationen** lassen sich jetzt über zwei Schaltflächen Logo und Icon aus der Mediathek auswählen. Falls schon ein Website-Icon eingestellt wurde, gibt es an derselben Stelle stattdessen die Schaltfläche **Bild wechseln**.

![Screenshot Customizer](/doc/customizer-1.png "Customizer")

Für das Logo empfiehlt sich das Format SVG, für das Website-Icon PNG.

Klickt man auf einen der beiden Buttons öffnet sich anschließend die Mediathek, in der man dann das gewünschte Bild auswählt. Dieses kann nach dem Auswählen noch über den Button **Bild zuschneiden** zugeschnitten werden.

Dabei sollte man sich nach Möglichkeit an die voreingestellte Fenstergröße (der nicht ausgegraute Bereich) halten, damit das Bild an der entsprechenden Stelle auch richtig passt.

![Screenshot Customizer](/doc/customizer-2.png "Customizer")

Hinweis:
Nachdem ein Bild zugeschnitten wurde, wird es als neues/separates Bild gespeichert. Wenn man versucht, ein bereits zugeschnittenes oder zu kleines Bild neu zuzuschneiden, erscheint eine Fehlermeldung.

## Footer / Widgets
Um Widgets zu bearbeiten und einzufügen, muss man zunächst auf die entsprechende Seite navigieren. Diese befindet sich in der Seitenleiste unter **Design** und dann im Untermenü bei **Widgets**.

Im Abschnitt **Verfügbare Widgets** gibt es eine Auswahl der verschiedenen Widgets, die dann per Drag-and-Drop in die Kästen mit den entsprechenden Positionen gezogen werden. Dort erscheinen dann die Optionen zum Konfigurieren des Widgets.

![Screenshot Widgets](/doc/widgets.png "Widgets")

**Standard Konfiguration für Widgets:**
- Responsive Footer Column
  1.  Widget: Text
  Titel: leer
  Inhalt: ```[button type="tertiary" size="large"]Mitglied Werden[/button]```
- Footer Column 1
leer
- Footer Column 2
  1. Widget: Text
  Titel: leer
  Inhalt: ```[social_link type="facebook" url="https://www.facebook.com/Wirtschaftsjunioren" text="Facebook"]```
  2. Widget: Text
  Titel: leer
  Inhalt: ```[social_link type="twitter" url="https://twitter.com/WJDeutschland" text="Twitter"]```
- Footer Column 3
  1. Widget: Text
  Titel: Interesse geweckt?
  Inhalt: ```[button type="tertiary" size="large"]Mitglied werden[/button]```
  2. Widget: Text (optional)
  Titel: leer
  Inhalt: Zwei Partnerbilder
- Header Column Login
  1. Widget: Text
  Titel: leer
  Inhalt: ```[icon_link icon="sign-in" url="/xyz" text="login"]```
- Header Column CTA
  1. Widget: Text
  Titel: leer
  Inhalt: ```[button type="tertiary" size="large"]Mitglied werden[/button]```

## Menüs
Den Menü-Editor erreichen Sie in WordPress über die linke Seitenleiste. Klicken oder überfahren Sie mit der Maus den Menüpunkt **Design** und wählen Sie anschließend **Menüs** aus.

![Screenshot Menü](/doc/setup-menu-1.png "Menü")

Anschließend wird Ihnen der Menü-Editor angezeigt. Hier haben Sie die Möglichkeit, die zuvor importierte Menüstruktur zu bearbeiten und nach Belieben anzupassen. Sie können aber auch von Grund auf neu anfangen und ein Menü nach Ihrer Vorstellung erstellen.

![Screenshot Menü-Editor](/doc/setup-menu-2.png "Menü-Editor")

**Aufbau des Menü-Editors:**
1.	Unter diesem Tab haben Sie die Möglichkeit, ein Menü auszuwählen, ein neues Menü zu erstellen oder ein vorhandenes Menü zu bearbeiten.
2.	Unter diesem Tab haben Sie die Möglichkeit, die Menüanordnung zu verändern.
3.	Unter Ansicht anpassen können Sie verschiedene Elemente und Optionen für den Menü-Editor aktivieren. Sie können beispielsweise einen Eintrag in einem neuen Tab öffnen lassen oder verschiedene Beitragstypen dem Menü hinzufügen. 
So können etwa Projekte, Positionen, Kategorien, Veranstaltungen, etc. in das Menü platziert werden.
4.	Unter dem Punkt Hilfe erhalten Sie eine von WordPress bereitgestellte Hilfestellung.
5.	Hier können Sie ein Menü auswählen, welches Sie bearbeiten möchten.
6.	An dieser Stelle haben Sie die Möglichkeit, ein neues Menü anzulegen.
7.	Hier können Sie neue Menüeinträge hinzufügen. Wählen Sie dafür das gewünschte Element aus. Nach Klick auf eins der Elemente erscheinen weitere Einstellungen. Unter “Seiten” wählen Sie alle Seiten aus, die Sie im Menü platzieren möchten. Unter individuelle Links können Sie eine URL angeben, die z.B. auf eine andere Seite verweist.
8.	An dieser Stelle werden alle hinzugefügten Menüeinträge angezeigt. Diese können Sie per Drag & Drop anordnen, bearbeiten oder aber auch löschen. Sie können an dieser Stelle ebenso die Menüposition festlegen, das Menü umbenennen oder aber auch löschen.

**Untertitel / Beschreibung für Menüpunkte**
Es ist möglich, den Menüpunkten zusätzlich eine Beschreibung zu geben, die dann auch im Menü angezeigt wird.
Diese Funktion ist standardmäßig deaktiviert und muss erst aktiviert werden.

![Screenshot Menü-Editor](/doc/setup-menu-3.png "Menü-Editor")

Dazu klickt man im Menü-Editor ganz rechts oben auf der Seite auf **Ansicht anpassen** und wählt im sich aufklappenden Fenster bei **Erweiterte Menüeigenschaften anzeigen** die Checkbox für die **“Beschreibung”** aus.

Anschließend ist das neue Textfeld bei jedem Menüpunkt sichtbar.



# Einstieg in den Blockeditor
## Einführung
Der Block-Editor in WordPress ist das Herzstück der Anwendung. Mit diesem ist es möglich, mittels verschiedener Blöcke, den Seitenaufbau einfach und strukturiert zu erstellen oder zu bearbeiten.

Den Block-Editor finden Sie immer dann vor, wenn Sie eine Seite oder einen Beitrag erstellen oder bearbeiten. Wenn Sie zum ersten Mal eine Seite oder einen Beitrag anlegen, dann begrüßt Sie der Editor mit einem Willkommens-Guide, welcher im Groben die Funktionalität und den Umgang mit dem Block-Editor beschreibt.

![Screenshot Gutenberg](/doc/gutenberg.png "Gutenberg")

**Aufbau des Block-Editors:**
1.	Mit Klick auf das WordPress Logo gelangen Sie zurück zum Dashboard von WordPress und verlassen den Editor.
2.	Über das + - Icon können Sie Blöcke der Seite hinzufügen.
3.	Hier finden Sie schnell Aktionen, um etwa Änderungen rückgängig zu machen. Zudem können Sie hier zwischen den Block-Ansichten wechseln.
4.	Über diesen Button können Sie die Seite oder den Beitrag veröffentlichen.
5.	Über die drei Punkte gelangen Sie zu Einstellungen, die Ihnen ermöglichen, den Block-Editor vom Aussehen her anzupassen.
6.	Unter dem Punkt Seite finden Sie seitenspezifische Einstellungen, um etwa den Status der Seite (öffentlich oder privat) umzustellen oder um ein Beitragsbild festzulegen.
7.	Unter dem Punkt Block finden Sie, zum jeweils ausgewählten Block, unterschiedliche Einstellungen, um etwa die Schriftgröße oder -farbe anzupassen.
8.	In diesem Bereich finden Sie den Inhalt Ihrer Seite bzw. Ihres Beitrags. Neue Blöcke werden in diesem Bereich abgelegt und können dann bearbeitet werden.

## Blockbeschreibungen
### Text-Blöcke
| Block | Beschreibung | Backend | Frontend |
|---|---|---|---|
|**Sektion**|Der Block Sektion unterteilt die Seite in verschiedene Teile und wird benutzt, um Blöcke innerhalb einer Seite aufzuteilen. **Es müssen dabei immer alle anderen Blöcke, bis auf den Hero-Block, innerhalb einer Sektion sein.** Der Hero-Block zählt als eigene Sektion und wird daher nicht in eine Sektion verpackt.|||
|**Button**|Der Button fungiert als speziell gestylter Link. Es können Button-Text und Link definiert werden.||![Screenshot Button Frontend](/doc/button-frontend.png "Button Frontend")|
|**Karte, Beitrags-Karte und Projekte-Karte**|Mit diesem Block wird ein Fenster mit Bild, Text und verschiedenen anderen Informationen eingebunden. Bei “Karte” können diese Informationen direkt in den Block eingegeben werden. Bei Beiträgen oder Projekten gilt der Block als Vorschau, sieht aber gleich aus. Die Informationen sind dabei schon aus dem Beitrag oder Projekt festgelegt und können nicht mehr geändert werden. Zusätzlich gibt es noch ein Häkchen, mit dem man einen “Mehr lesen”-Button für die Vorschau aktivieren kann||![Screenshot Karte Frontend](/doc/card-frontend.png "Karte Frontend")|
|**Download**|Der Block Download gibt Ihnen die Möglichkeit, eine Datei wie etwa ein PDF-Dokument, als Download auf der Seite anzubieten. Laden Sie dafür die Datei in die Mediathek hoch und wählen Sie sie anschließend aus.|![Screenshot Download Backend](/doc/download-backend.png "Download Backend")|![Screenshot Download Frontend](/doc/download-frontend.png "Karte Frontend")|
|**Zitat**|Der Zitatblock gibt Ihnen die Möglichkeit, eine Aussage zu zitieren. Dieser Block wird auf der Seite entsprechend formatiert und Sie können zusätzlich die Person angeben, welche Sie zitieren möchten.||![Screenshot Zitat Frontend](/doc/quote-frontend.png "Zitat Frontend")|
|**Hero**|Der Hero-Block ist standardmäßig auf jeder Seite vorzufinden. Hier definieren Sie den Titel der Seite, einen Untertitel und können ein Headerbild festlegen. Es ist möglich, mehrere Bilder auszuwählen. Diese werden auf der Seite als Bild-Slider dargestellt.|![Screenshot Hero Backend](/doc/hero-backend.png "Hero Backend")||
|**Absatz**|Im Absatz-Block können Sie einen Textabschnitt definieren. In der Seitenleiste stehen Ihnen Optionen für die Schriftgröße oder Schriftfarbe zur Verfügung.|![Screenshot Absatz Backend](/doc/paragraph-backend.png "Absatz Backend")||
|**Überschrift**|Der Block Überschrift bietet Ihnen ein Feld, um eine Überschrift zu definieren. In der Seitenleiste finden Sie Optionen, um beispielsweise die Schriftgröße sowie Schriftfarbe anzupassen. Eine Überschrift sollte idealerweise vor einem Text stehen und einen aussagekräftigen Titel tragen.|![Screenshot Überschrift Backend](/doc/heading-backend.png "Überschrift Backend")||
|**Liste**|Der Listen-Block bietet Ihnen die Möglichkeit, eine Aufzählung verschiedener Standpunkte aufzustellen.|![Screenshot Liste Backend](/doc/list-backend.png "Liste Backend")||
|**Classic**|Der Inhalt dieses Blocks wird als klassischer Wordpress Inhalt gewertet. Mit klassisch ist hiermit ein veralteter Editor gemeint, in dem per HTML der Seiteninhalt geschrieben wurde. In diesem Editor lassen sich also HTML Code, Text sowie Shortcodes einfügen.|||

### Medien-Blöcke
**Bild, Galerie und Video**
Mit diesen 3 Blöcken lassen sich einzelne Bilder, Galerien oder Videos auf der Seite einfügen. Die Elemente müssen dabei zuerst in die Mediathek der Seite hochgeladen und dann innerhalb der Blocks ausgewählt werden.

### Design-Blöcke
**Spalten**
Die Spalten dienen zum Teilen des Layouts. Das Einfügen mehrerer Spalten teilt die Seite vertikal. Pro Spalte kann dann die Breite in Prozent angegeben werden. Dabei sollte beim Addieren der Werte immer am Ende 100% herauskommen. Also z.B. 33,3% / 66,6%

**Abstandshalter**
Der Abstandshalter macht nichts weiter, als einen Abstand zwischen dem darüber und dem darunter liegenden Block einzustellen. Er kann auch nicht weiter angepasst werden.


### Widgets-Blöcke

**Contact-Form-7**
Mit diesem Block wird ein Formular, welches mit Contact-Form-7 gebaut wurde, eingebunden.

![Screenshot Block Contact-Form-7](/doc/block-cform.png "Block Contact-Form-7")

**Formular (GravityForms)**
Über den Formular-Block lässt sich ein in GravityForms konfiguriertes Formular auf der Seite einfügen. Dabei muss das gewünschte Formular ausgewählt werden.

![Screenshot Block GravityForms](/doc/block-gravityform.png "Block GravityForms")

**Shortcodes**
Als Shortcode bezeichnet man einen in eckigen Klammern geschriebenen Code, der von WP als Befehl zum Ausgeben von bestimmten Inhalten interpretiert wird. Damit lassen sich z.B. verschiedene Events anzeigen. 

Beispiel:
```
[events_list category='internationales-kultur' limit=2][/events_list]
```

**Social-Icons**
Der Social-Icons-Block besteht selbst wiederum aus weiteren Mini-Blöcken, den eigentlichen Icons. Diese fügt man innerhalb des Blocks mit dem +-Button hinzu. Für diese lassen sich Labels und Links anpassen. Der gesamte Block hat auch mehrere Einstellungsmöglichkeiten wie Farbe, Position etc.

![Screenshot Liste Backend](/doc/block-social-icons.png "Liste Backend")

# Shortcodes
## Button
|Attribut|Werte|
|---|---|
|type|unbreakable, inverted, secondary, secondary-plain, tertiary, tertiary-plain, solid, inverted-solid, secondary-solid, secondary-solid-inverted, tertiary-solid, close, sharp|
|size|large, small, tiny, wide|

**Beispiel:**
```
[button type="tertiary" size="large"]Mitglied werden[/button]
```

## Social-Link
|Attribut|Werte|
|---|---|
|type|facebook, twitter, instagram, youtube, linkedin (vgl. fontawesome)|
|url|Beliebiger URL auf die verwiesen werden soll|
|text|Beliebiger Text welcher angezeigt werden soll|

**Beispiel:**
```
[social_link type="facebook" url="https://www.facebook.com/Wirtschaftsjunioren" text="Facebook"]
```

## Icon-Link
|Attribut|Werte|
|---|---|
|icon|Beliebiger Font-Awesome Icon Name, eine Liste mit allen Möglichkeiten finden Sie hier: https://fontawesome.com/icons|
|url|Beliebiger URL auf die verwiesen werden soll|
|text|Beliebiger Text welcher angezeigt werden soll|

**Beispiel:**
```
[icon_link icon="sign-in" url="/login" text="login"]
```

## Latest-News (Startseite)
Um die Darstellung der Latest News auf der Startseite in der bekannten Stufenform zu realisieren, ist der Shortcode in einer Sektion zu platzieren, welche unter den Blockeinstellungen **Erweitert** die zusätzliche CSS-Klasse **news** gesetzt ist.

**Beispiel:**
```
[latest_news]
```

# Plugins

## Borlabs Cookie (lizenzpflichtig, lizensierbar über WJD)

Das Cookie Consent Plugin Borlabs Cookie ist über eine Agentur-Lizenz der Wirtschaftsjunioren Deutschland e.V. für WJ-Organisationen kostenfrei nutzbar. Um eine Lizenz anzufordern bitte ein Ticket unter https://plattform.wjd.de/fuer-kreise/wordpress-design/ mit Angabe des Kreises und der Domain öffnen. 

### Grundkonfiguration

Nach der Installation des Plugins im Backend zum **Borlabs Cookie** Menüpunkt navigieren und anschließend das Untermenü **Import & Export** aufrufen. Bei den Import-Feldern folgende Inhalte einfügen

#### Allgemeine & Darstellungs Einstellungen
```json
{"config":{"cookieStatus":true,"cookieBeforeConsent":false,"aggregateCookieConsent":false,"cookiesForBots":true,"respectDoNotTrack":false,"reloadAfterConsent":false,"jQueryHandle":"jquery","metaBox":[],"automaticCookieDomainAndPath":true,"cookieDomain":"","cookiePath":"\/","cookieLifetime":365,"crossDomainCookie":[],"showCookieBox":true,"showCookieBoxOnLoginPage":false,"cookieBoxIntegration":"javascript","cookieBoxBlocksContent":true,"cookieBoxRefuseOptionType":"button","cookieBoxHideRefuseOption":true,"privacyPageId":0,"privacyPageURL":"","privacyPageCustomURL":"","imprintPageId":0,"imprintPageURL":0,"imprintPageCustomURL":"","hideCookieBoxOnPages":[],"supportBorlabsCookie":false,"cookieBoxShowAcceptAllButton":true,"cookieBoxIgnorePreSelectStatus":true,"cookieBoxLayout":"bar-advanced","cookieBoxPosition":"bottom-center","cookieBoxCookieGroupJustification":"space-around","cookieBoxAnimation":true,"cookieBoxAnimationDelay":false,"cookieBoxAnimationIn":"fadeInUp","cookieBoxAnimationOut":"fadeOutDown","cookieBoxShowLogo":false,"cookieBoxLogo":"http:\/\/wjd2020.localdev\/wp-content\/plugins\/borlabs-cookie\/images\/borlabs-cookie-logo.svg","cookieBoxLogoHD":"http:\/\/wjd2020.localdev\/wp-content\/plugins\/borlabs-cookie\/images\/borlabs-cookie-logo.svg","cookieBoxFontFamily":"inherit","cookieBoxFontSize":14,"cookieBoxBgColor":"#fff","cookieBoxTxtColor":"#555","cookieBoxAccordionBgColor":"#f7f7f7","cookieBoxAccordionTxtColor":"#555","cookieBoxTableBgColor":"#fff","cookieBoxTableTxtColor":"#555","cookieBoxTableBorderColor":"#eee","cookieBoxBorderRadius":4,"cookieBoxBtnBorderRadius":4,"cookieBoxCheckboxBorderRadius":4,"cookieBoxAccordionBorderRadius":0,"cookieBoxTableBorderRadius":0,"cookieBoxBtnFullWidth":true,"cookieBoxBtnColor":"#f8a102","cookieBoxBtnHoverColor":"#41d4ae","cookieBoxBtnTxtColor":"#ffffff","cookieBoxBtnHoverTxtColor":"#ffffff","cookieBoxRefuseBtnColor":"#f8a102","cookieBoxRefuseBtnHoverColor":"#41d4ae","cookieBoxRefuseBtnTxtColor":"#555555","cookieBoxRefuseBtnHoverTxtColor":"#555","cookieBoxAcceptAllBtnColor":"#f8a102","cookieBoxAcceptAllBtnHoverColor":"#41d4ae","cookieBoxAcceptAllBtnTxtColor":"#fff","cookieBoxAcceptAllBtnHoverTxtColor":"#fff","cookieBoxBtnSwitchActiveBgColor":"#f8a102","cookieBoxBtnSwitchInactiveBgColor":"#bdc1c8","cookieBoxBtnSwitchActiveColor":"#fff","cookieBoxBtnSwitchInactiveColor":"#fff","cookieBoxBtnSwitchRound":true,"cookieBoxCheckboxInactiveBgColor":"#fff","cookieBoxCheckboxInactiveBorderColor":"#f8a102","cookieBoxCheckboxActiveBgColor":"#f8a102","cookieBoxCheckboxActiveBorderColor":"#41d4ae","cookieBoxCheckboxDisabledBgColor":"#e6e6e6","cookieBoxCheckboxDisabledBorderColor":"#e6e6e6","cookieBoxCheckboxCheckMarkActiveColor":"#fff","cookieBoxCheckboxCheckMarkDisabledColor":"#999","cookieBoxPrimaryLinkColor":"#f8a102","cookieBoxPrimaryLinkHoverColor":"#41d4ae","cookieBoxSecondaryLinkColor":"#aaa","cookieBoxSecondaryLinkHoverColor":"#aaa","cookieBoxRejectionLinkColor":"#888","cookieBoxRejectionLinkHoverColor":"#888","cookieBoxCustomCSS":"","cookieBoxTextHeadline":"Datenschutzeinstellungen","cookieBoxTextDescription":"Wir nutzen Cookies auf unserer Website. Einige von ihnen sind essenziell, w\u00e4hrend andere uns helfen, diese Website und Ihre Erfahrung zu verbessern.","cookieBoxTextAcceptButton":"Ich akzeptiere","cookieBoxTextManageLink":"Individuelle Datenschutzeinstellungen","cookieBoxTextRefuseLink":"Nur essenzielle Cookies akzeptieren","cookieBoxTextCookieDetailsLink":"Cookie-Details","cookieBoxTextPrivacyLink":"Datenschutzerkl\u00e4rung","cookieBoxTextImprintLink":"Impressum","cookieBoxPreferenceTextHeadline":"Datenschutzeinstellungen","cookieBoxPreferenceTextDescription":"Hier finden Sie eine \u00dcbersicht \u00fcber alle verwendeten Cookies. Sie k\u00f6nnen Ihre Einwilligung zu ganzen Kategorien geben oder sich weitere Informationen anzeigen lassen und so nur bestimmte Cookies ausw\u00e4hlen.","cookieBoxPreferenceTextSaveButton":"Speichern","cookieBoxPreferenceTextAcceptAllButton":"Alle akzeptieren","cookieBoxPreferenceTextRefuseLink":"Nur essenzielle Cookies akzeptieren","cookieBoxPreferenceTextBackLink":"Zur\u00fcck","cookieBoxPreferenceTextSwitchStatusActive":"An","cookieBoxPreferenceTextSwitchStatusInactive":"Aus","cookieBoxPreferenceTextShowCookieLink":"Cookie-Informationen anzeigen","cookieBoxPreferenceTextHideCookieLink":"Cookie-Informationen ausblenden","cookieBoxCookieDetailsTableAccept":"Akzeptieren","cookieBoxCookieDetailsTableName":"Name","cookieBoxCookieDetailsTableProvider":"Anbieter","cookieBoxCookieDetailsTablePurpose":"Zweck","cookieBoxCookieDetailsTablePrivacyPolicy":"Datenschutzerkl\u00e4rung","cookieBoxCookieDetailsTableHosts":"Host(s)","cookieBoxCookieDetailsTableCookieName":"Cookie Name","cookieBoxCookieDetailsTableCookieExpiry":"Cookie Laufzeit","cookieBoxConsentHistoryTableDate":"Datum","cookieBoxConsentHistoryTableVersion":"Version","cookieBoxConsentHistoryTableConsents":"Einwilligungen","contentBlockerHostWhitelist":[],"removeIframesInFeeds":true,"contentBlockerFontFamily":"inherit","contentBlockerFontSize":14,"contentBlockerBgColor":"#000","contentBlockerTxtColor":"#fff","contentBlockerBgOpacity":80,"contentBlockerBtnBorderRadius":4,"contentBlockerBtnColor":"#f8a102","contentBlockerBtnHoverColor":"#41d4ae","contentBlockerBtnTxtColor":"#fff","contentBlockerBtnHoverTxtColor":"#fff","contentBlockerLinkColor":"#f8a102","contentBlockerLinkHoverColor":"#41d4ae","testEnvironment":true}}
```

#### Cookies & Cookie Gruppen
```json
{"cookieGroups":[{"id":"1","group_id":"essential","language":"de","name":"Essenziell","description":"Essenzielle Cookies erm\u00f6glichen grundlegende Funktionen und sind f\u00fcr die einwandfreie Funktion der Website erforderlich.","pre_selected":"1","position":"1","status":"1","undeletable":"1"},{"id":"2","group_id":"statistics","language":"de","name":"Statistiken","description":"Statistik Cookies erfassen Informationen anonym. Diese Informationen helfen uns zu verstehen, wie unsere Besucher unsere Website nutzen.","pre_selected":"1","position":"2","status":"1","undeletable":"1"},{"id":"3","group_id":"marketing","language":"de","name":"Marketing","description":"Marketing-Cookies werden von Drittanbietern oder Publishern verwendet, um personalisierte Werbung anzuzeigen. Sie tun dies, indem sie Besucher \u00fcber Websites hinweg verfolgen.","pre_selected":"1","position":"3","status":"1","undeletable":"1"},{"id":"4","group_id":"external-media","language":"de","name":"Externe Medien","description":"Inhalte von Videoplattformen und Social-Media-Plattformen werden standardm\u00e4\u00dfig blockiert. Wenn Cookies von externen Medien akzeptiert werden, bedarf der Zugriff auf diese Inhalte keiner manuellen Einwilligung mehr.","pre_selected":"1","position":"4","status":"1","undeletable":"1"}],"cookies":[{"cookie_id":"borlabs-cookie","language":"de","cookie_group_id":"1","service":"Custom","name":"Borlabs Cookie","provider":"Eigent\u00fcmer dieser Website","purpose":"Speichert die Einstellungen der Besucher, die in der Cookie Box von Borlabs Cookie ausgew\u00e4hlt wurden.","privacy_policy_url":"","hosts":"a:0:{}","cookie_name":"borlabs-cookie","cookie_expiry":"1 Jahr","opt_in_js":"","opt_out_js":"","fallback_js":"","settings":"a:2:{s:25:\"blockCookiesBeforeConsent\";s:1:\"0\";s:10:\"prioritize\";s:1:\"0\";}","position":"1","status":"1","undeletable":"1"},{"cookie_id":"facebook","language":"de","cookie_group_id":"4","service":"Custom","name":"Facebook","provider":"Facebook","purpose":"Wird verwendet, um Facebook-Inhalte zu entsperren.","privacy_policy_url":"https:\/\/www.facebook.com\/privacy\/explanation","hosts":"a:1:{i:0;s:13:\".facebook.com\";}","cookie_name":"","cookie_expiry":"","opt_in_js":"<script>if(typeof window.BorlabsCookie === \"object\") { window.BorlabsCookie.unblockContentId(\"facebook\"); }<\/script>","opt_out_js":"","fallback_js":"","settings":"a:2:{s:25:\"blockCookiesBeforeConsent\";s:1:\"0\";s:10:\"prioritize\";s:1:\"0\";}","position":"1","status":"0","undeletable":"0"},{"cookie_id":"googlemaps","language":"de","cookie_group_id":"4","service":"Custom","name":"Google Maps","provider":"Google","purpose":"Wird zum Entsperren von Google Maps-Inhalten verwendet.","privacy_policy_url":"https:\/\/policies.google.com\/privacy","hosts":"a:1:{i:0;s:11:\".google.com\";}","cookie_name":"NID","cookie_expiry":"6 Monate","opt_in_js":"<script>if(typeof window.BorlabsCookie === \"object\") { window.BorlabsCookie.unblockContentId(\"googlemaps\"); }<\/script>","opt_out_js":"","fallback_js":"","settings":"a:2:{s:25:\"blockCookiesBeforeConsent\";s:1:\"0\";s:10:\"prioritize\";s:1:\"0\";}","position":"2","status":"0","undeletable":"0"},{"cookie_id":"instagram","language":"de","cookie_group_id":"4","service":"Custom","name":"Instagram","provider":"Facebook","purpose":"Wird verwendet, um Instagram-Inhalte zu entsperren.","privacy_policy_url":"https:\/\/www.instagram.com\/legal\/privacy\/","hosts":"a:1:{i:0;s:14:\".instagram.com\";}","cookie_name":"pigeon_state","cookie_expiry":"Sitzung","opt_in_js":"<script>if(typeof window.BorlabsCookie === \"object\") { window.BorlabsCookie.unblockContentId(\"instagram\"); }<\/script>","opt_out_js":"","fallback_js":"","settings":"a:2:{s:25:\"blockCookiesBeforeConsent\";s:1:\"0\";s:10:\"prioritize\";s:1:\"0\";}","position":"3","status":"0","undeletable":"0"},{"cookie_id":"openstreetmap","language":"de","cookie_group_id":"4","service":"Custom","name":"OpenStreetMap","provider":"OpenStreetMap Foundation","purpose":"Wird verwendet, um OpenStreetMap-Inhalte zu entsperren.","privacy_policy_url":"https:\/\/wiki.osmfoundation.org\/wiki\/Privacy_Policy","hosts":"a:1:{i:0;s:18:\".openstreetmap.org\";}","cookie_name":"_osm_location, _osm_session, _osm_totp_token, _osm_welcome, _pk_id., _pk_ref., _pk_ses., qos_token","cookie_expiry":"1-10 Jahre","opt_in_js":"<script>if(typeof window.BorlabsCookie === \"object\") { window.BorlabsCookie.unblockContentId(\"openstreetmap\"); }<\/script>","opt_out_js":"","fallback_js":"","settings":"a:2:{s:25:\"blockCookiesBeforeConsent\";s:1:\"0\";s:10:\"prioritize\";s:1:\"0\";}","position":"4","status":"0","undeletable":"0"},{"cookie_id":"twitter","language":"de","cookie_group_id":"4","service":"Custom","name":"Twitter","provider":"Twitter","purpose":"Wird verwendet, um Twitter-Inhalte zu entsperren.","privacy_policy_url":"https:\/\/twitter.com\/privacy","hosts":"a:2:{i:0;s:10:\".twimg.com\";i:1;s:12:\".twitter.com\";}","cookie_name":"__widgetsettings, local_storage_support_test","cookie_expiry":"Unbegrenzt","opt_in_js":"<script>if(typeof window.BorlabsCookie === \"object\") { window.BorlabsCookie.unblockContentId(\"twitter\"); }<\/script>","opt_out_js":"","fallback_js":"","settings":"a:2:{s:25:\"blockCookiesBeforeConsent\";s:1:\"0\";s:10:\"prioritize\";s:1:\"0\";}","position":"5","status":"0","undeletable":"0"},{"cookie_id":"vimeo","language":"de","cookie_group_id":"4","service":"Custom","name":"Vimeo","provider":"Vimeo","purpose":"Wird verwendet, um Vimeo-Inhalte zu entsperren.","privacy_policy_url":"https:\/\/vimeo.com\/privacy","hosts":"a:1:{i:0;s:16:\"player.vimeo.com\";}","cookie_name":"vuid","cookie_expiry":"2 Jahre","opt_in_js":"<script>if(typeof window.BorlabsCookie === \"object\") { window.BorlabsCookie.unblockContentId(\"vimeo\"); }<\/script>","opt_out_js":"","fallback_js":"","settings":"a:2:{s:25:\"blockCookiesBeforeConsent\";s:1:\"0\";s:10:\"prioritize\";s:1:\"0\";}","position":"6","status":"0","undeletable":"0"},{"cookie_id":"youtube","language":"de","cookie_group_id":"4","service":"Custom","name":"YouTube","provider":"YouTube","purpose":"Wird verwendet, um YouTube-Inhalte zu entsperren.","privacy_policy_url":"https:\/\/policies.google.com\/privacy","hosts":"a:1:{i:0;s:10:\"google.com\";}","cookie_name":"NID","cookie_expiry":"6 Monate","opt_in_js":"<script>if(typeof window.BorlabsCookie === \"object\") { window.BorlabsCookie.unblockContentId(\"youtube\"); }<\/script>","opt_out_js":"","fallback_js":"","settings":"a:2:{s:25:\"blockCookiesBeforeConsent\";s:1:\"0\";s:10:\"prioritize\";s:1:\"0\";}","position":"7","status":"0","undeletable":"0"}]}
```

#### Content Blocker
```json
{"contentBlocker":[{"id":"1","content_blocker_id":"facebook","language":"de","name":"Facebook","description":"","privacy_policy_url":"https:\/\/www.facebook.com\/privacy\/explanation","hosts":"a:2:{i:0;s:12:\"facebook.com\";i:1;s:20:\"connect.facebook.net\";}","preview_html":"<div class=\"_brlbs-content-blocker\">\n\t<div class=\"_brlbs-embed _brlbs-facebook\">\n    \t<img class=\"_brlbs-thumbnail\" src=\"%%thumbnail%%\" alt=\"%%name%%\">\n\t\t<div class=\"_brlbs-caption\">\n\t\t\t<p>Mit dem Laden des Beitrags akzeptieren Sie die Datenschutzerkl\u00e4rung von Facebook.<br><a href=\"%%privacy_policy_url%%\" target=\"_blank\" rel=\"nofollow noopener noreferrer\">Mehr erfahren<\/a><\/p>\n\t\t\t<p><a class=\"_brlbs-btn\" href=\"#\" data-borlabs-cookie-unblock role=\"button\">Beitrag laden<\/a><\/p>\n\t\t\t<p><label><input type=\"checkbox\" name=\"unblockAll\" value=\"1\" checked> <small>Facebook-Beitr\u00e4ge immer entsperren<\/small><\/label><\/p>\n\t\t<\/div>\n\t<\/div>\n<\/div>","preview_css":".BorlabsCookie ._brlbs-facebook {\n    border: 1px solid #e1e8ed;\n    border-radius: 6px;\n\tmax-width: 516px;\n\tpadding: 3px 0;\n}\n\n.BorlabsCookie ._brlbs-facebook a._brlbs-btn {\n\tbackground: #4267b2;\n\tborder-radius: 2px;\n}\n\n.BorlabsCookie ._brlbs-facebook a._brlbs-btn:hover {\n\tbackground: #3b5998;\n}\n","global_js":"","init_js":"if(typeof FB === \"object\") { FB.XFBML.parse(el.parentElement); }","settings":"a:1:{s:33:\"executeGlobalCodeBeforeUnblocking\";b:0;}","status":"1","undeletable":"1"},{"id":"2","content_blocker_id":"default","language":"de","name":"Standard","description":"Der <strong><em>Standard<\/em> Content Blocker<\/strong> ist ein spezieller Typ, der immer dann verwendet wird, wenn kein bestimmter <strong>Content Blocker<\/strong> gefunden wurde.<br> Daher ist es nicht m\u00f6glich, die Funktion <strong>Alle freischalten<\/strong> zu verwenden.","privacy_policy_url":"","hosts":"a:0:{}","preview_html":"<div class=\"_brlbs-content-blocker\">\n    <div class=\"_brlbs-default\">\n        <p>Klicken Sie auf den unteren Button, um den Inhalt von %%name%% zu laden.<\/p>\n        <p><a class=\"_brlbs-btn\" href=\"#\" data-borlabs-cookie-unblock role=\"button\">Inhalt laden<\/a><\/p>\n    <\/div>\n<\/div>","preview_css":"","global_js":"","init_js":"","settings":"a:1:{s:33:\"executeGlobalCodeBeforeUnblocking\";b:0;}","status":"1","undeletable":"1"},{"id":"3","content_blocker_id":"googlemaps","language":"de","name":"Google Maps","description":"","privacy_policy_url":"https:\/\/policies.google.com\/privacy","hosts":"a:2:{i:0;s:15:\"maps.google.com\";i:1;s:20:\"www.google.com\/maps\/\";}","preview_html":"<div class=\"_brlbs-content-blocker\">\n\t<div class=\"_brlbs-embed _brlbs-google-maps\">\n    \t<img class=\"_brlbs-thumbnail\" src=\"%%thumbnail%%\" alt=\"%%name%%\">\n\t\t<div class=\"_brlbs-caption\">\n\t\t\t<p>Mit dem Laden der Karte akzeptieren Sie die Datenschutzerkl\u00e4rung von Google.<br><a href=\"%%privacy_policy_url%%\" target=\"_blank\" rel=\"nofollow noopener noreferrer\">Mehr erfahren<\/a><\/p>\n\t\t\t<p><a class=\"_brlbs-btn\" href=\"#\" data-borlabs-cookie-unblock role=\"button\">Karte laden<\/a><\/p>\n\t\t\t<p><label><input type=\"checkbox\" name=\"unblockAll\" value=\"1\" checked> <small>Google Maps immer entsperren<\/small><\/label><\/p>\n\t\t<\/div>\n\t<\/div>\n<\/div>","preview_css":".BorlabsCookie ._brlbs-google-maps a._brlbs-btn {\n\tbackground: #4285f4;\n\tborder-radius: 3px;\n}\n\n.BorlabsCookie ._brlbs-google-maps a._brlbs-btn:hover {\n\tbackground: #fff;\n\tcolor: #4285f4;\n}","global_js":"","init_js":"","settings":"a:1:{s:33:\"executeGlobalCodeBeforeUnblocking\";b:0;}","status":"1","undeletable":"1"},{"id":"4","content_blocker_id":"instagram","language":"de","name":"Instagram","description":"","privacy_policy_url":"https:\/\/www.instagram.com\/legal\/privacy\/","hosts":"a:1:{i:0;s:13:\"instagram.com\";}","preview_html":"<div class=\"_brlbs-content-blocker\">\n\t<div class=\"_brlbs-embed _brlbs-instagram\">\n    \t<img class=\"_brlbs-thumbnail\" src=\"%%thumbnail%%\" alt=\"%%name%%\">\n\t\t<div class=\"_brlbs-caption\">\n\t\t\t<p>Mit dem Laden des Beitrags akzeptieren Sie die Datenschutzerkl\u00e4rung von Instagram.<br><a href=\"%%privacy_policy_url%%\" target=\"_blank\" rel=\"nofollow noopener noreferrer\">Mehr erfahren<\/a><\/p>\n\t\t\t<p><a class=\"_brlbs-btn\" href=\"#\" data-borlabs-cookie-unblock role=\"button\">Beitrag laden<\/a><\/p>\n\t\t\t<p><label><input type=\"checkbox\" name=\"unblockAll\" value=\"1\" checked> <small>Instagram-Beitr\u00e4ge immer entsperren<\/small><\/label><\/p>\n\t\t<\/div>\n\t<\/div>\n<\/div>","preview_css":".BorlabsCookie ._brlbs-instagram {\n    border: 1px solid #e1e8ed;\n    border-radius: 6px;\n\tmax-width: 516px;\n\tpadding: 3px 0;\n}\n\n\n.BorlabsCookie ._brlbs-instagram a._brlbs-btn {\n\tbackground: #3897f0;\n\tborder-radius: 4px;\n}\n\n.BorlabsCookie ._brlbs-instagram a._brlbs-btn:hover {\n\tbackground: #117ee4;\n}\n","global_js":"","init_js":"if (typeof instgrm === \"object\") { instgrm.Embeds.process(); }","settings":"a:1:{s:33:\"executeGlobalCodeBeforeUnblocking\";b:0;}","status":"1","undeletable":"1"},{"id":"5","content_blocker_id":"openstreetmap","language":"de","name":"OpenStreetMap","description":"","privacy_policy_url":"https:\/\/wiki.osmfoundation.org\/wiki\/Privacy_Policy","hosts":"a:1:{i:0;s:21:\"www.openstreetmap.org\";}","preview_html":"<div class=\"_brlbs-content-blocker\">\n\t<div class=\"_brlbs-embed\">\n    \t<img class=\"_brlbs-thumbnail\" src=\"%%thumbnail%%\" alt=\"%%name%%\">\n\t\t<div class=\"_brlbs-caption\">\n\t\t\t<p>Mit dem Laden der Karte akzeptieren Sie die Datenschutzerkl\u00e4rung von OpenStreetMap Foundation.<br><a href=\"%%privacy_policy_url%%\" target=\"_blank\" rel=\"nofollow noopener noreferrer\">Mehr erfahren<\/a><\/p>\n\t\t\t<p><a class=\"_brlbs-btn\" href=\"#\" data-borlabs-cookie-unblock role=\"button\">Karte laden<\/a><\/p>\n\t\t\t<p><label><input type=\"checkbox\" name=\"unblockAll\" value=\"1\" checked> <small>OpenStreetMaps immer entsperren<\/small><\/label><\/p>\n\t\t<\/div>\n\t<\/div>\n<\/div>","preview_css":"","global_js":"","init_js":"","settings":"a:1:{s:33:\"executeGlobalCodeBeforeUnblocking\";b:0;}","status":"1","undeletable":"1"},{"id":"6","content_blocker_id":"twitter","language":"de","name":"Twitter","description":"","privacy_policy_url":"https:\/\/twitter.com\/privacy","hosts":"a:2:{i:0;s:11:\"twitter.com\";i:1;s:4:\"t.co\";}","preview_html":"<div class=\"_brlbs-content-blocker\">\n\t<div class=\"_brlbs-embed _brlbs-twitter\">\n    \t<img class=\"_brlbs-thumbnail\" src=\"%%thumbnail%%\" alt=\"%%name%%\">\n\t\t<div class=\"_brlbs-caption\">\n\t\t\t<p>Mit dem Laden des Tweets akzeptieren Sie die Datenschutzerkl\u00e4rung von Twitter.<br><a href=\"%%privacy_policy_url%%\" target=\"_blank\" rel=\"nofollow noopener noreferrer\">Mehr erfahren<\/a><\/p>\n\t\t\t<p><a class=\"_brlbs-btn\" href=\"#\" data-borlabs-cookie-unblock role=\"button\">Inhalt laden<\/a><\/p>\n\t\t\t<p><label><input type=\"checkbox\" name=\"unblockAll\" value=\"1\" checked> <small>Twitter Tweets immer entsperren<\/small><\/label><\/p>\n\t\t<\/div>\n\t<\/div>\n<\/div>","preview_css":".BorlabsCookie ._brlbs-twitter {\n    border: 1px solid #e1e8ed;\n    border-radius: 3px;\n\tmax-width: 516px;\n}\n\n.BorlabsCookie ._brlbs-twitter a._brlbs-btn {\n\tbackground: #1da1f2;\n\tborder-radius: 0;\n}\n\n.BorlabsCookie ._brlbs-twitter a._brlbs-btn:hover {\n\tbackground: #fff;\n\tcolor: #1da1f2;\n}\n","global_js":"","init_js":"","settings":"a:1:{s:33:\"executeGlobalCodeBeforeUnblocking\";b:0;}","status":"1","undeletable":"1"},{"id":"7","content_blocker_id":"vimeo","language":"de","name":"Vimeo","description":"","privacy_policy_url":"https:\/\/vimeo.com\/privacy","hosts":"a:1:{i:0;s:9:\"vimeo.com\";}","preview_html":"<div class=\"_brlbs-content-blocker\">\n\t<div class=\"_brlbs-embed _brlbs-video-vimeo\">\n    \t<img class=\"_brlbs-thumbnail\" src=\"%%thumbnail%%\" alt=\"%%name%%\">\n\t\t<div class=\"_brlbs-caption\">\n\t\t\t<p>Mit dem Laden des Videos akzeptieren Sie die Datenschutzerkl\u00e4rung von Vimeo.<br><a href=\"%%privacy_policy_url%%\" target=\"_blank\" rel=\"nofollow noopener noreferrer\">Mehr erfahren<\/a><\/p>\n\t\t\t<p><a class=\"_brlbs-btn _brlbs-icon-play-white\" href=\"#\" data-borlabs-cookie-unblock role=\"button\">Video laden<\/a><\/p>\n\t\t\t<p><label><input type=\"checkbox\" name=\"unblockAll\" value=\"1\" checked> <small>Vimeo immer entsperren<\/small><\/label><\/p>\n\t\t<\/div>\n\t<\/div>\n<\/div>","preview_css":".BorlabsCookie ._brlbs-video-vimeo a._brlbs-btn {\n\tbackground: #00adef;\n\tborder-radius: 20px;\n}\n\n.BorlabsCookie ._brlbs-video-vimeo a._brlbs-btn:hover {\n\tbackground: #fff;\n\tcolor: #00adef;\n}\n\n.BorlabsCookie ._brlbs-video-vimeo a._brlbs-btn._brlbs-icon-play-white:hover::before {\n\tbackground: url(\"data:image\/svg+xml,%3Csvg version='1.1' xmlns='http:\/\/www.w3.org\/2000\/svg' xmlns:xlink='http:\/\/www.w3.org\/1999\/xlink' x='0' y='0' width='78' height='78' viewBox='0, 0, 78, 78'%3E%3Cg id='Layer_1'%3E%3Cg%3E%3Cpath d='M7.5,71.5 L7.5,7.5 L55.5,37.828 L7.5,71.5' fill='%2300adef'\/%3E%3Cpath d='M7.5,71.5 L7.5,7.5 L55.5,37.828 L7.5,71.5' fill-opacity='0' stroke='%2300adef' stroke-width='12' stroke-linecap='round' stroke-linejoin='round'\/%3E%3C\/g%3E%3C\/g%3E%3C\/svg%3E\") no-repeat center;\n\tbackground-size: contain;\n\tcontent: \" \";\n}\n","global_js":"","init_js":"","settings":"a:3:{s:33:\"executeGlobalCodeBeforeUnblocking\";b:0;s:14:\"saveThumbnails\";b:0;s:12:\"videoWrapper\";b:0;}","status":"1","undeletable":"1"},{"id":"8","content_blocker_id":"youtube","language":"de","name":"YouTube","description":"","privacy_policy_url":"https:\/\/policies.google.com\/privacy","hosts":"a:4:{i:0;s:8:\"youtu.be\";i:1;s:20:\"youtube-nocookie.com\";i:2;s:8:\"youtube.\";i:3;s:11:\"youtube.com\";}","preview_html":"<div class=\"_brlbs-content-blocker\">\r\n\t<div class=\"_brlbs-embed _brlbs-video-youtube\">\r\n    \t<img class=\"_brlbs-thumbnail\" src=\"%%thumbnail%%\" alt=\"%%name%%\">\r\n\t\t<div class=\"_brlbs-caption\">\r\n\t\t\t<p>Mit dem Laden des Videos akzeptieren Sie die Datenschutzerkl\u00e4rung von YouTube.<br><a href=\"%%privacy_policy_url%%\" target=\"_blank\" rel=\"nofollow noopener noreferrer\">Mehr erfahren<\/a><\/p>\r\n\t\t\t<p><a class=\"_brlbs-btn _brlbs-icon-play-white\" href=\"#\" data-borlabs-cookie-unblock role=\"button\">Video laden<\/a><\/p>\r\n\t\t\t<p><label><input type=\"checkbox\" name=\"unblockAll\" value=\"1\" checked> <small>YouTube immer entsperren<\/small><\/label><\/p>\r\n\t\t<\/div>\r\n\t<\/div>\r\n<\/div>","preview_css":".BorlabsCookie ._brlbs-video-youtube a._brlbs-btn {\r\n\tbackground: #ff0000;\r\n\tborder-radius: 20px;\r\n}\r\n\r\n.BorlabsCookie ._brlbs-video-youtube a._brlbs-btn:hover {\r\n\tbackground: #fff;\r\n\tcolor: red;\r\n}\r\n\r\n.BorlabsCookie ._brlbs-video-youtube a._brlbs-btn._brlbs-icon-play-white:hover::before {\r\n\tbackground: url(\"data:image\/svg+xml,%3Csvg version='1.1' xmlns='http:\/\/www.w3.org\/2000\/svg' xmlns:xlink='http:\/\/www.w3.org\/1999\/xlink' x='0' y='0' width='78' height='78' viewBox='0, 0, 78, 78'%3E%3Cg id='Layer_1'%3E%3Cg%3E%3Cpath d='M7.5,71.5 L7.5,7.5 L55.5,37.828 L7.5,71.5' fill='%23ff0000'\/%3E%3Cpath d='M7.5,71.5 L7.5,7.5 L55.5,37.828 L7.5,71.5' fill-opacity='0' stroke='%23ff0000' stroke-width='12' stroke-linecap='round' stroke-linejoin='round'\/%3E%3C\/g%3E%3C\/g%3E%3C\/svg%3E\") no-repeat center;\r\n\tbackground-size: contain;\r\n\tcontent: \" \";\r\n}\r\n","global_js":"","init_js":"","settings":"a:6:{s:10:\"unblockAll\";s:1:\"0\";s:14:\"saveThumbnails\";s:1:\"0\";s:16:\"thumbnailQuality\";s:13:\"maxresdefault\";s:19:\"changeURLToNoCookie\";s:1:\"1\";s:12:\"videoWrapper\";s:1:\"0\";s:33:\"executeGlobalCodeBeforeUnblocking\";s:1:\"0\";}","status":"1","undeletable":"1"}]}
```

#### Script Blocker
```json
{"scriptBlocker":[]}
```

Anschließend per **Importieren** den Import abschließen.

### Weitere Konfiguration
1. Lizenz einfügen
2. Testmodus deaktivieren
3. Im Menüpunkt **Cookie-Box** die Datenschutz und Impressum Seite auswählen

### Weiterführende Konfiguration
Die weiterführende Konfiguration ist abhängig von den verwendeten Funktionen und Abhängigkeiten. Hierzu zählt der Content/Script Blocker. Empfehlenswert wären z.B. Google Maps, YouTube und Vimeo, da die direkt über den PageBuilder kommen können.

### Haftungsausschluss
Die rechtlichen Informationen, rechtsgültigen Vorlagen und gesetzlichen Vorschriften sind keine Rechtsberatung. Die Wirtschaftsjunioren Deutschland e.V. übernehmen keine Haftung für den rechtssicheren Betrieb der Webseite auf Basis dieser Dokumentation.

## Contact Form 7 (kostenfrei)

Ein Kontaktformular wird z.B. wie folgt erstellt:

```
[form_row type="radio" error-name="Anrede"][radio anrede use_label_element default:1 "Frau" "Herr"][/form_row]

[form_row label="Vorname*" error-name="Vorname"][text* namefirst][/form_row] [form_row label="Nachname*" error-name="Nachname"][text* namelast][/form_row] [form_row label="E-Mail*" error-name="E-Mail"][email* email][/form_row]				

[form_row label="Unternehmen/Organisation*" error-name="Unternehmen/Organisation"][text* enterprise][/form_row]

[form_row label="Position (optional)"][text position][/form_row]

[form_row label="Geburtsdatum" error-name="Geburtstadtum"][date* birthdate placeholder "tt.mm.jjjj"][/form_row]

[form_row label="Wunschkreis (Optional)"][select circle "- Nicht festgelegt/ausgewählt -" "Altmark" "Altötting" "Amberg-Sulzbach" "Anhalt-Bitterfeld"][/form_row]
 
[form_row label="Nachricht"][textarea message][/form_row]

[form_row type="checkbox" error-name="Datenschutzerklärung"][acceptance dsgvo] Ich akzeptiere die Datenschutzerklärung. [/acceptance][/form_row]

[form_row][submit "Senden"][/form_row]
```

Wie hier zu erkennen ist, muss jeder reguläre Input noch in einen [form_row] Shortcode gepackt werden.
 								
Dieser Shortcode kann die Parameter “type”, “label” und “error-name” annehmen. Type und Label sollten selbsterklärend sein. Type wird nur bei Checkboxen und Radio Inputs benötigt. Error-Name definiert die Ausgabe für die Fehlermeldungen im oberen Bereich. 
Alternativ lässt sich auch im Backend im Menüpunkt “Formular” (mit dem Brief-Icon) ein Formular anlegen. Man muss im Shortcode dann nur noch die ID aus dem Backend angeben z.B.: [contact-form-7 id="136460" title="Contact form 1"]. Den Shortcode inkl. ID kann man im Backend unter dem eben genannten Punkt finden.
Wenn ein Formular angelegt wurde, kann man zusätzlich auch den dafür vorgesehenen Block “Contact Form 7” verwenden. In diesem muss nur das gewünschte Formular ausgewählt werden.
Weitere Informationen (auf englisch): https://contactform7.com/docs/



## GravityForms (kostenpflichtig)

Um alle Funktionen von GravityForms Pro nutzen zu können, muss ein Lizenzschlüssel eingegeben werden.

GravityForms funktioniert ähnlich wie Contact Form 7. Im Backend gibt es für das Plugin ebenfalls einen Punkt “Formulare” (wenn man Contact Form 7 und GravityForms gleichzeitig installiert hat, kann das ein bisschen verwirrend sein). Mit dem Import sollten schon 2 Formulare angelegt worden sein. Es gibt folgenden Funktionen / Menüpunkte:

- Im Tab “Bearbeiten” lassen sich verschiedene Felder per Drag-and-Drop nach links in den Formularbereich ziehen und dort anpassen.
-	Der Tab “Einstellungen” hat vier weitere Menüs:
    -	Die “Formulareinstellungen” legen das grundlegende Design und Verhalten des Formulars fest.
    -	In den “Bestätigungen” kann festgelegt werden, welche Nachricht nach dem Absenden des Formulars angezeigt wird.
    -	“Benachrichtigungen” betrifft die verschiedenen Mails, die nach dem Ausfüllen abgeschickt werden. Dabei kann man sowohl den Empfänger als auch den Seitenbesucher mit verschiedenen Nachrichten angeben.
    - Bei den “Personenbezogenen” Daten lassen sich noch zusätzliche Einstellungen zur Speicherung eben dieser treffen.
-	Im Bereich “Einträge” kann man die Daten von allen bisher abgeschickten Feldern einsehen und nach Bedarf auch erneut Benachrichtigungen verschicken.
-	Mit der “Vorschau” kann man sich eine grobe Vorschau des Formulars anzeigen lassen.

Um die Formulare auf einer Seite anzeigen zu lassen, wählt man im Block-Editor den Block “Formular” im Bereich “Einbettungen”, siehe Abschnitt “Einstieg in den Block-Editor” -> “Einbettungen-Blöcke” 

Weitere Informationen (auf englisch): https://docs.gravityforms.com/


## My Calendar (kostenfrei)
Für das My Calendar Plugin müssen einige Einstellungen getroffen werden, nachdem das Plugin installiert wurde.			
- Unter **Kategorien verwalten** sollten die entsprechenden Kategorien, die benötigt werden, erstellt und mit Farben versehen werden.
- Im Style-Editor das Häkchen bei **Die My Calendar Styledatei deaktivieren** setzen
- In den **Einstellungen > Allgemein** die Hauptkalender-Seiten-ID auf die Seite der Events setzen.
- In den **Einstellungen > Text**
  - den Veranstaltungstitel (Kacheln) auf **{title}** setzen
  - den Veranstaltungstitel (Einzeln) auf **{title}** setzen
  - den Veranstaltungstitel (Liste) auf **{title}** setzen
  - das Label für Ganztages Veranstaltungen auf **Ganztags** setzen
  - das Primary Date Format auf “**j. F Y**” setzen
  - das Datum im Kachelmodus in der Wochenansicht auf ”**j**“ setzen			 			
- In den Einstellungen > Ausgabe
  - das Häkchen bei **Open calendar links to event details** setzen
  - **Reihenfolge des Kalenderlayouts wie folgt setzen:**			
    - Primäre vor/zurück Schaltflächen
    - Schalte zwischen Tages-, Wochen und Monatsansicht um
    - Der Kalender
    - Rest in beliebiger Reihenfolge unter den schwarzen Balken Elements below here will be hidden ziehen	

Danach kann der Kalender mit dem Shortcode ```[my_calendar]``` eingebunden werden

Weitere Informationen (auf Englisch): https://docs.joedolson.com/my-calendar/		

## Events Manager (kostenpflichtig)

### Allgemein
Das Plugin Events-Manager ist unter dem Menüpunkt “Veranstaltungen” zu finden.

![Screenshot Veranstaltungen](/doc/events-1.png "Veranstaltungen")

- Eine Übersicht über alle angelegten Veranstaltungen gibt es im Menüpunkt “Veranstaltungen”. Von dort aus können auch die weiteren Funktionen des entsprechenden Events ausgewählt werden bzw. Events bearbeitet werden.
- Unter dem Menüpunkt “Veranstaltungen hinzufügen” lassen sich neue Veranstaltungen anlegen.
- “Veranstaltungsschlagworte“ und “Veranstaltungskategorien” lassen sich anlegen, um die Events zu gruppieren bzw. einzuordnen. Über diese beiden Menüpunkte kann man diese einsehen und bearbeiten. Beim Anlegen oder Bearbeiten einer Veranstaltung werden diese dann zugeordnet.
- Für die Veranstaltungsorte gilt dasselbe wie für die Kategorien und Schlagworte. Diese werden ebenfalls beim Anlegen von Veranstaltungen angelegt bzw. zugeordnet.
- “Wiederkehrende Veranstaltungen” sind praktisch identisch zu den normalen Veranstaltungen. Es gibt lediglich ein neues Feld, in das man eingibt, nach welcher Zeitspanne die Veranstaltung wieder stattfindet.
- Hat man seiner Veranstaltung eine Buchung zugefügt, so kann man sich beim Punkt “Buchungen” alle Einträge anschauen.
- In den “Einstellungen” werden verschiedene generelle Einstellungen für das Plugin getroffen.
- Im “Formulareditor” werden die Anmelde- und Buchungsformulare angepasst, die bei Bedarf einem Event hinzugefügt werden können.
- Die “Zahlungs-Gateways” bestimmen die Zahlungsoptionen und die Zahlungsweise der Buchungsformulare. Alle Einstellungen dazu können hier getätigt werden.
- In der “Gutscheinverwaltung” werden Gutscheine für die Buchungen angelegt. Hat eine Veranstaltung, die einen gültigen Gutschein besitzt, einen Preis, dann wird automatisch ein Gutscheinfeld angezeigt, in das die Gutscheincodes eingetragen werden können.

### Event anlegen

![Screenshot Veranstaltungen](/doc/events-2.png "Veranstaltungen")

1.	Hier wird der Titel der Veranstaltung eingegeben.
2.	Veranstaltungsbeschreibung, Informationen etc. werden hier eingegeben.
3.	Der Typ des Veranstaltungsortes wird hier eingegeben. Je nach Typ gibt es verschiedene Optionen.
4.	Kreuzt man das Häkchen an, wird ein zuvor erstelltes Buchungsformular für die Veranstaltung aktiviert. Die zugehörigen Einstellungen klappen dann auf.
5.	Hier kann man einen Zeitpunkt für die Veranstaltung festlegen.
6.	Hier kann man die Schlagworte eingeben.
7.	Hier kann man verschiedene Kategorien auswählen.
8.	Unterhalb lässt sich das Beitragsbild festlegen.

### Shortcodes
Events haben automatisch eine eigene Seite, auf die per Link weitergeleitet werden kann.
Es gibt aber auch die Möglichkeit, eine kleine Vorschau auf einer Seite einzufügen.

Dazu wird der Block Shortcode ausgewählt und dort dann die Shortcodes eingefügt (siehe Abschnitt Block-Editor).

Für eine Liste von Events wird der Shortcode event_list benutzt.

```
[events_list limit="10" location="restaurant-zum-hirschen" category='adventurebox-karlsruhe']
```

Mit dem Parameter limit lässt sich die maximale Anzahl an Events einstellen.
Parameter location gibt den Veranstaltungsort an und gibt damit nur noch Events an diesem Standort aus.
Dasselbe lässt sich auch noch mit den Kategorien machen, hier heißt der Parameter category.

Zu beachten ist hier, dass bei category die Titelform der Kategorie benutzt werden muss. Diese kann man in der Übersicht der Kategorien sehen.
Beim Standort muss es die korrekte URL-Version sein. Diese lässt sich beim Bearbeiten des entsprechenden Ortes als letzte Zeichenkette der URL auslesen:

![Screenshot Veranstaltungen](/doc/events-3.png "Veranstaltungen")

Um nur eine Event-Vorschau anzeigen zu lassen, benutzt man den Shortcode event.

```
[event post_id="135918"]
```

Hier muss als Parameter die ID des Events eingegeben werden.

Diese lässt sich in der URL ablesen, wenn man gerade eine Veranstaltung anlegt / bearbeitet:

![Screenshot Veranstaltungen](/doc/events-4.png "Veranstaltungen")

Bei beiden Shortcodes sieht die Vorschau des Events dann ungefähr so aus:

![Screenshot Veranstaltungen](/doc/events-5.png "Veranstaltungen")

### Darstellungseinstellungen

Um die Darstellung der verschiedenen Veranstaltungsseiten und -Listen and das allgemeine Design anzupassen, kann unter Einstellungen -> Formatierung eigenes HTML eingefügt werden. Hier sind ein paar Vorschläge, die für eine grundlegende Darstellung benutzt werden können.

#### Veranstaltungen -> Standardkopfzeilen für eine Veranstaltungsliste
```
<div class="section__content">
```

#### Veranstaltungen -> Standardformat der Veranstaltungsliste
```
<div class="views-row">
	<div class="card v-card has-credit has-cta has-image has-link" data-url="#_EVENTURL">
		<div class="card__content">
			<div class="card__main">
				<h3 class="card__headline">#_EVENTNAME</h3>
				<ul class="card__meta-data no-list-style">
					<li class="card__meta-data-item is-date" style="display: block !important;">Datum:  #_EVENTDATES<br></li>
					<li class="card__meta-data-item is-date" style="display: block !important;">Uhrzeit: #_EVENTTIMES</li>
					<li class="card__meta-data-item is-date"></li>
				</ul>
				<div class="rte-content v-margin-collapse">
					#_EVENTEXCERPT
				</div>
			</div>

			<div class="card__footer">
				<a href="#_EVENTURL" ref="cta" class="card__cta is-link" target="_self"></a>
				<a href="#_EVENTURL">#_EVENTNAME</a>
				<i class="card__icon  fas fa-chevron-right" aria-hidden="true"></i>
			</div>
		</div>
	</div>
</div>
```

#### Veranstaltungen -> Inhalt der Fußzeile für die Veranstaltungsliste
```
</div>
```

#### Veranstaltungen -> Seitenformatierung für einzelne Veranstaltung
```
<div class="container">
	<div class="v-hero hero has-max-width mb-xxl">
		<div class="hero__box  is-highlighted" style="margin-top: 0; width: 100%;">
			<h1 class="hero__headline">#_EVENTNAME</h1>
		</div>
	</div>
	<div class="section is-regular">
		<div class="section__content text-left">
			<div class="card is-regular has-no-meta is-mirrored has-credit has-image has-no-footer">
				<div class="card__image-wrapper order-1"></div>
				<div class="card__content">
					<div class="card__main">
						<div class="rte-content v-margin-collapse">
							<h3 class="card__headline">#_EVENTNAME</h3>
							<p>
								<strong>Datum/Zeit</strong><br>
								Termin - #_EVENTDATES<br><i>#_EVENTTIMES</i>
							</p>
							{has_location}
								<p>
									<strong>Veranstaltungsort</strong><br/>
									#_LOCATIONLINK
								</p>
							{/has_location}
							<p>
								<strong>Kategorien</strong>
							</p>
							#_CATEGORIES
							<p></p>
						</div>
					</div>
				</div>
			</div>
			<br style="clear:both">
			<div class="headline-section__content text-center container  pt-40">
				<h2 class="headline-section__headline is-highlighted">VERANSTALTUNGS&shy;DETAILS</h2>
			</div>
			<div class="card is-regular has-no-meta has-no-image has-no-footer mb-5">
				<div class="card__content">
					<div class="card__main">
						<div class="rte-content v-margin-collapse">
							#_EVENTNOTES
						</div>
					</div>
				</div>
			</div>

			{has_bookings}
				<div class="headline-section__content text-center container  pt-40">
					<h2 class="headline-section__headline is-highlighted">Buchungen</h2>
				</div>
				#_BOOKINGFORM
				<br class="clear" style="clear:left;">
			{/has_bookings}
										
		</div>
	</div>
</div>
```

### Veranstaltungsorte -> Seitenformatierung für einzelne Veranstaltungsort
```
<div class="card is-regular has-no-meta has-no-footer">
	<div class="card__content">
		<div class="card__main">
			<div style="float:right; margin:0px 0px 15px 15px;">#_LOCATIONMAP</div>
			<p>
				<strong>Adresse</strong><br/>
				#_LOCATIONADDRESS<br/>
				#_LOCATIONTOWN<br/>
				#_LOCATIONSTATE<br/>
				#_LOCATIONREGION<br/>
				#_LOCATIONPOSTCODE<br/>
				#_LOCATIONCOUNTRY
			</p>
			<div class="rte-content v-margin-collapse">
				#_LOCATIONNOTES
			</div>
			
			<h3>Kommende Veranstaltungen</h3>
			<p>#_LOCATIONNEXTEVENTS</p>				
		</div>
	</div>
</div>
```

#### Veranstaltungskategorien -> Seitenformatierung für einzelne Kategorie
```
<div class="card is-regular has-no-meta has-no-footer">
	<div class="card__content">
		<div class="card__main">
			<div class="rte-content v-margin-collapse">
				#_CATEGORYNOTES
			</div>
			
			<h3>Kommende Veranstaltungen</h3>
			<p>#_CATEGORYNEXTEVENTS</p>				
		</div>
	</div>
</div>
```

Weitere Informationen (auf Englisch): https://wp-events-plugin.com/documentation/

## Suche
Seit Version 1.2.0 unterstützt das Design "Suche". Die Suche ist standardmäßig nicht eingeblendet sondern ist über das Administrationsbackend im Punkt "Einstellungen" -> "Allgemein" unter der Rubrik Optionen für globale Suche zu aktivieren.

Die Suche funktioniert mit der integrierten WordPress Suche - für ein optimales Benutzererlebnis wird jedoch relevanssi (Free, bevorzugt Premium) empfohlen.

## One-Click-Demo-Import
Dieses Plugin ermöglicht es, per Button, verschiedene Demo Daten in WordPress einzufügen.
Wenn das Plugin eingerichtet wurde, muss man lediglich im WP-Menü auf “Design” -> “Import Demo Data” gehen und dort den Button “Import Demo Data” anklicken. Nach einigen Sekunden ist der Vorgang abgeschlossen und die neuen Inhalte auf der Seite sichtbar. Siehe auch Abschnitt oben “Installation”.

**Konfiguration (für Entwickler):**
Die hier beschriebene Konfiguration bezieht sich auf den aktuellen Stand des WJ-Themes. In anderen Themes sind die entsprechenden Filter an unterschiedlichen Stellen zu finden.

- Im Theme ist das Plugin aktuell so eingestellt, dass die gesamten Import-Informationen aus einer Datei kommen: /demo-import/demo-data.xml. In dieser Datei kann man die importierten Daten im XML-Format angeben. Das geht am leichtesten, indem man in einem bestehenden WP im Menü die Funktion “Werkzeuge” -> “Daten Exportieren” aufruft und dort die entsprechenden Daten als XML-Datei abspeichert. Den Inhalt der Datei kann man dann in die demo-data.xml einfügen.
- Eine zusätzliche Konfiguration lässt sich in der  /demo-import/demo-data.php anpassen. Dort werden nach dem Import die importierten Menüs an die entsprechende Position gesetzt und die Start- und Beitragsseite gesetzt.

Weitere Informationen (auf englisch): https://github.com/awesomemotive/one-click-demo-import

# Entwickler-Dokumentation
## Ordnerstruktur
- Calendar > Alle Erweiterungen zu Plugins ein eigenem Folder
- Core > Alle Basisfunktionen (hier würde ich dein Code sehen)
- Shortcodes > Basisshortcodes oder nach Plugins geteilt
- TemplatePart > Teile von Inhalten (MenuWalker bisher, falls nötig content Templates...)
- Utility > Alles was kleine Teile zur Erweiterung von Templates sind
- templates > Alles was mit WP Templating zu tun hat (Inhalte index.php ...)