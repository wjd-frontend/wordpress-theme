/* eslint-disable linebreak-style */
/* eslint-disable no-undef */
jQuery(document).ready(function () {
    /**
     * Global Vars
     */
    var body = jQuery('body');
    /**
     * Apply IE Styles
     */
    if (getInternetExplorerVersion() !== -1) {
        jQuery('body').addClass('ie');
        if (jQuery(window).width() > 768) {
            var cards = jQuery('.card');
            cards.each(function (k, value) {
                value = jQuery(value);
                // eslint-disable-next-line no-undef
                imagesLoaded(value, function () {
                    value = value.find('img');
                    if (value.length > 0) {
                        var valueParent = value.parent();
                        if (value.outerWidth() / value.outerHeight() > valueParent.outerWidth() / valueParent.outerHeight()) {
                            value.css('max-height', '100%');
                        } else {
                            value.css('max-width', '100%');
                            valueParent.css('min-height', value.outerHeight());
                        }
                    }
                });
            });
        }
    }
    /**
     * Nav Menus
     */
    var mainNavItems = jQuery('.main-nav__item.menu-item-has-children');
    var subNav = jQuery('.header__sub-nav-wrapper');
    if (mainNavItems.length > 0) {
        mainNavItems.click(function (e) {
            e.preventDefault();
            var target = jQuery(e.target);
            var link = target.find('>a');
            var clickedIndex = target.index() + 1;
            if (link.hasClass('is-selected')) {
                //Only Collapse current Menu
                link.removeClass('is-selected');
                subNav.removeClass('is-open');
                subNav.find('.is-selected').removeClass('is-selected');
            } else {
                //Check if there is another menu open, if so close it
                if (mainNavItems.find('.is-selected').length > 0) {
                    mainNavItems.find('.is-selected').removeClass('is-selected');
                    subNav.find('.is-selected').removeClass('is-selected');
                }
                //Open current menu
                subNav.addClass('is-open');
                link.addClass('is-selected');
                subNav.find('.sub-nav:nth-child(' + clickedIndex + ')').addClass('is-selected');
            }
        });
        jQuery('.header__close').click(function () {
            mainNavItems.find('.is-selected').removeClass('is-selected');
            subNav.removeClass('is-open');
            subNav.find('.is-selected').removeClass('is-selected');
        });
    }
    var mobileNav = jQuery('.mobile-nav');
    if (mobileNav.length > 0) {
        var jsHeader = jQuery('.js-header');
        var headerToggle = jQuery('.header__toggle');
        headerToggle.click(function () {
            if (jsHeader.hasClass('is-open')) {
                jsHeader.removeClass('is-open');
                jsHeader.addClass('is-closed');
                headerToggle.removeClass('is-active');
            } else {
                jsHeader.removeClass('is-closed');
                jsHeader.addClass('is-open');
                headerToggle.addClass('is-active');
            }
        });
        var menuLinks = mobileNav.find('li.mobile-nav__item');
        menuLinks.each(function (k, value) {
            value = jQuery(value);
            var valueLink = value.find('>a');
            value.click(function (e) {
                var target = jQuery(e.target);
                e.stopPropagation();
                if (value.hasClass('is-not-selected')) {
                    value.removeClass('is-not-selected');
                    valueLink.removeClass('is-not-selected');
                    value.addClass('is-selected');
                    valueLink.addClass('is-selected');
                    jQuery('body, html').animate({
                        scrollTop: value.offset().top
                    }, 400);
                } else {
                    if (target.hasClass('is-selected')) {
                        value.removeClass('is-selected');
                        valueLink.removeClass('is-selected');
                        value.addClass('is-not-selected');
                        valueLink.addClass('is-not-selected');
                    }
                }
            });
        });

        mobileNav.find('.menu-item-has-children.is-level-1 > a').each(function (k, itemValue) {
            itemValue = jQuery(itemValue);
            var itemText = itemValue.clone().children().remove().end().text();
            itemValue.parent().find('> nav > ul').prepend(
                jQuery('<li>', { class: 'parent-link' }).append(
                    jQuery('<a>', { class: 'parent-link mobile-nav__item-link is-not-selected is-level-2', href: itemValue.attr('href'), text: itemText })
                )
            );
        });
    }
    /**
     * Hero Swiper
     */
    var heroSwiper = jQuery('.hero-swiper');
    if (heroSwiper.length > 0) {
        if (heroSwiper.find('.swiper-slide').length > 1) {
            new Swiper('.hero-swiper', {
                direction: 'horizontal',
                effect: 'slide',
                freeModeSticky: true,
                slidesPerView: 1,
                loop: true,
                centeredSlides: true,
                freeMode: false,
                pagination: {
                    el: '.hero__pagination',
                    clickable: true,
                    renderBullet: function (index, className) {
                        return '<li class="hero__pagination-item ' + className + '"></li>';
                    }
                },
                autoplay: {
                    delay: 5000,
                    disableOnInteraction: false
                },
            });
        }
    }
    /**
     * Image Gallery Lightbox
     */
    var imageGallery = jQuery('.image-gallery.is-desktop');
    if (imageGallery.length > 0) {
        imageGallery.each(function (k, swiperValue) {
            swiperValue = jQuery(swiperValue);
            swiperValue.find('.gallery-swiper').addClass('gallery-swiper-' + k);
            var overlay = swiperValue.find('.image-gallery-overlay');
            var gallerySwiper = new Swiper('.gallery-swiper-' + k, {
                direction: 'horizontal',
                loop: true,
                autoplay: {
                    delay: 5000,
                    disableOnInteraction: false
                },
            });
            swiperValue.find('.image-gallery__thumbnail').each(function (k, value) {
                value = jQuery(value);
                value.click(function () {
                    var openIndex = value.data('index-image');
                    gallerySwiper.slideTo(openIndex, 0, false);
                    overlay.removeClass('invis');
                    body.addClass('is-modal-open');
                });
            });
            swiperValue.find('.image-gallery__thumbnail-redirect').click(function () {
                swiperValue.find('.image-gallery__thumbnail').first().trigger('click');
            });
            overlay.find('.image-gallery-overlay__close').click(function () {
                overlay.addClass('invis');
                body.removeClass('is-modal-open');
            });
            overlay.find('.image-gallery-overlay__next').click(function () {
                gallerySwiper.slideNext();
            });
            overlay.find('.image-gallery-overlay__prev').click(function () {
                gallerySwiper.slidePrev();
            });
        });
    }
    var mobileImageGallery = jQuery('.image-gallery.is-mobile');
    if (mobileImageGallery.length > 0) {
        mobileImageGallery.each(function (k, mobGalValue) {
            mobGalValue = jQuery(mobGalValue);
            mobGalValue.find('.swiper-container').addClass('gallery-swiper-' + k);
            mobGalValue.find('.toggle').click(function (e) {
                e.preventDefault();
                var slidesJSON = JSON.parse(mobGalValue.attr('data-images'));
                var swiperWrapper = mobGalValue.find('.swiper-wrapper');
                swiperWrapper.empty();
                jQuery.each(slidesJSON, function (k, JSONvalue) {
                    if (JSONvalue.src) {
                        swiperWrapper.append('<div class="image-gallery__slide swiper-slide"><div class="image-gallery__thumbnail"><figure class="figure"><div class="figure__image-wrapper is-highlighted"><img src="' + JSONvalue.src + '" class="figure__image"></div><figcaption class="figure__caption"><div class="image-gallery__caption caption"><div class="image-gallery__caption-meta caption__meta">' + JSONvalue.meta + '</div><div class="image-gallery__caption-title caption__title">' + JSONvalue.caption + '</div></div></figcaption></figure></div></div>');
                    } else {
                        swiperWrapper.append('<div class="image-gallery__slide swiper-slide"><div class="image-gallery__thumbnail"><figure class="figure"><div class="figure__image-wrapper is-highlighted"><div class="figure__image">' + JSONvalue.image + '</div></div><figcaption class="figure__caption"><div class="image-gallery__caption caption"><div class="image-gallery__caption-meta caption__meta">' + JSONvalue.meta + '</div><div class="image-gallery__caption-title caption__title">' + JSONvalue.caption + '</div></div></figcaption></figure></div></div>');
                    }
                });
                new Swiper('.gallery-swiper-' + k, {
                    direction: 'horizontal',
                    loop: true,
                    autoplay: {
                        delay: 5000,
                        disableOnInteraction: false
                    },
                });
            });
        });
    }
    /**
     * JS Webforms
     */
    var jsWebform = jQuery('.js-webform, .wpcf7-form');
    if (jsWebform.length > 0) {
        jsWebform.each(function (k, formValue) {
            formValue = jQuery(formValue);
            var requiredInputsMap = [];
            formValue.find('input[required], input[aria-required="true"]').each(function (k, reqValue) {
                reqValue = jQuery(reqValue);
                requiredInputsMap.push({ value: reqValue, name: reqValue.attr('name') });
                reqValue.removeAttr('required');
            });
            formValue.find('textarea[required], textarea[aria-required="true"]').each(function (k, reqValue) {
                reqValue = jQuery(reqValue);
                requiredInputsMap.push({ value: reqValue, name: reqValue.attr('name') });
                reqValue.removeAttr('required');
            });
            formValue.submit(function (e) {
                e.preventDefault();
                var failedInputs = [];
                formValue.find('.is-invalid').removeClass('is-invalid');
                jQuery(requiredInputsMap).each(function (key) {
                    if (requiredInputsMap[key].value.attr('type') === 'radio') {
                        var sameNameFilter = requiredInputsMap.filter(function (x) {
                            return x.name === requiredInputsMap[key].name;
                        });
                        var isChecked = false;
                        jQuery(sameNameFilter).each(function (k, value) {
                            if (value.value.is(':checked')) {
                                isChecked = true;
                                return false;
                            }
                        });
                        if (!isChecked) {
                            requiredInputsMap[key].value.closest('.form-item').addClass('is-invalid');
                            requiredInputsMap[key].value.closest('.js-webform-radios').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.closest('.webform-flex--container').data('error'));
                        }
                    } else if (requiredInputsMap[key].value.attr('type') === 'checkbox') {
                        if (requiredInputsMap[key].value.is(':checked')) {
                            return false;
                        } else {
                            requiredInputsMap[key].value.closest('.js-form-type-checkbox').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.closest('.webform-flex--container').data('error'));
                        }
                    } else {
                        if (requiredInputsMap[key].value.val().length <= 0) {
                            requiredInputsMap[key].value.closest('.form-item').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.closest('.webform-flex--container').data('error'));
                        }
                    }
                });
                failedInputs = jQuery.uniqueSort(failedInputs);
                jQuery('.error-list').remove();
                if (failedInputs.length > 0) {
                    var errors = jQuery('<div>', { class: 'error-list' });
                    var errorList = jQuery('<ul>', { class: 'item-list__comma-list' });
                    if (failedInputs.length === 1) {
                        errors.text('1 Fehler wurde gefunden');
                    } else {
                        errors.text(failedInputs.length + ' Fehler wurden gefunden');
                    }
                    errors.append(errorList);
                    jQuery.each(failedInputs, function (k, errorValue) {
                        errorList.append(jQuery('<li>', { text: errorValue }));
                    });
                    formValue.prepend(errors);
                    jQuery('body, html').animate({
                        scrollTop: errors.offset().top
                    }, 200);
                } else {
                    formValue[0].submit();
                }
            });
        });
    }
    /**
     * Cookie Banner
     */
    var cookieBanner = jQuery('#cookie-banner');
    if (cookieBanner.length > 0) {
        if (!getCookie('cookie-consent')) {
            cookieBanner.show();
        }
        cookieBanner.find('.accept').click(function () {
            setCookie('cookie-consent', true, 365);
            cookieBanner.hide();
        });
        cookieBanner.find('.cookie-banner__close').click(function () {
            cookieBanner.hide();
        });
    }
    /**
     * Card Contets
     */
    var cardSections = jQuery('.section');
    if (cardSections.length > 0) {
        cardSections.each(function (k, value) {
            var cardContents = jQuery(value).find('.card.is-vertical .card__content');
            imagesLoaded(cardContents, function () {
                var maxHeight = 0;
                cardContents.each(function (k, value) {
                    value = jQuery(value);
                    var h = value.outerHeight();
                    if (h > maxHeight) {
                        maxHeight = h;
                    }
                });
                cardContents.each(function (k, value) {
                    value = jQuery(value);
                    value.css('height', maxHeight);
                });
            });
        });
    }
    var clickCards = jQuery('.card');
    if (clickCards.length > 0) {
        clickCards.each(function (k, value) {
            value = jQuery(value);
            if (value.data('url')) {
                value.click(function (e) {
                    e.preventDefault();
                    window.location = value.data('url');
                });
            }
        });
    }
    var eventWidget = jQuery('.event-widget');
    if (eventWidget.length > 0) {
        var closestRow = eventWidget.closest('.row');
        if (closestRow.children().length > 1) {
            var maxHeight = 610;
            imagesLoaded(closestRow, function () {
                closestRow.children().each(function (k, value) {
                    value = jQuery(value);
                    var valueCard = value.find('.card');
                    if (valueCard.length > 0) {
                        valueCardHeight = valueCard.height();
                        if (valueCardHeight > maxHeight) {
                            maxHeight = valueCardHeight;
                        }
                    }
                    eventWidget.closest('.row').children().each(function (k, value) {
                        value = jQuery(value);
                        var valueCard = value.find('.card');
                        if (valueCard.length > 0) {
                            valueCard.height(maxHeight + 5);
                        }
                    });
                });
            });
        }
    }
    /**
     * Calender Filter Nav
     */
    var eventOverviewHeader = jQuery('.event-overview-header');
    if (eventOverviewHeader.length > 0) {
        var weekdaysPrev = jQuery('.weekdays.prev');
        var weekdaysCurrent = jQuery('.weekdays.current');
        var weekdaysNext = jQuery('.weekdays.next');
        eventOverviewHeader.find('.week-nav .prev-week').click(function (e) {
            e.preventDefault();
            weekdaysPrev.show();
            weekdaysCurrent.hide();
            weekdaysNext.hide();
        });
        eventOverviewHeader.find('.week-nav .current-week').click(function (e) {
            e.preventDefault();
            weekdaysPrev.hide();
            weekdaysCurrent.show();
            weekdaysNext.hide();
        });
        eventOverviewHeader.find('.week-nav .next-week').click(function (e) {
            e.preventDefault();
            weekdaysPrev.hide();
            weekdaysCurrent.hide();
            weekdaysNext.show();
        });
    }
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = 'expires=' + d.toUTCString();
        cvalue = JSON.stringify(cvalue);
        document.cookie = cname + '=' + cvalue + ';' + expires + '; path=/';
    }
    function getCookie(cname) {
        var name = cname + '=';
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }

    /**
     * Check for IE (Better to be done at PHP Level)
     */
    function getInternetExplorerVersion() {
        var rV = -1;
        if (navigator.appName === 'Microsoft Internet Explorer' || navigator.appName === 'Netscape') {
            var uA = navigator.userAgent;
            var rE = new RegExp('MSIE ([0-9]{1,}[.0-9]{0,})');

            if (rE.exec(uA) != null) {
                rV = parseFloat(RegExp.$1);
            } else if (navigator.userAgent.match(/Trident.*rv:11\./)) {
                rV = 11;
            }
        }
        return rV;
    }

    /**
     * Event Calender
     */
    function updateCalendar(calendarType, vars) {
        var eventCalendar = jQuery('.mc-main');
        if (eventCalendar.length > 0) {
            var date = new Date();
            if (calendarType === 'month') {
                jQuery('.single-event').remove();
                jQuery('.week-headline').remove();
                jQuery('.list-headline').remove();
                eventCalendar.removeClass('is-list');
                eventCalendar.removeClass('is-week');
                eventCalendar.addClass('is-month');
                var currentMonth = eventCalendar.find('.my-calendar-nav .current-month');
                if (currentMonth.length > 0) {
                    currentMonth.remove();
                }
                eventCalendar.find('.my-calendar-nav .my-calendar-prev').after('<li class="current-month"><a href="' + window.location.origin + window.location.pathname + '?cid=my-calendar&month=' + date.toJSON().slice(5, 7) + '&yr=' + date.toJSON().slice(0, 4) + '">aktueller Monat</a></li>');
                eventCalendar.find('tbody .mc-row').each(function (k, row) {
                    row = jQuery(row);
                    var events = [];
                    row.find('.calendar-event').map(function () {
                        return jQuery(this).attr('class');
                    }).each(function (i, str) {
                        if (!~$.inArray(str, events)) {
                            events.push(str);
                        }
                    });
                    row.addClass('big-row-' + events.length);
                    if (events.length > 0) {
                        jQuery.each(events, function (k, value) {
                            value = '.' + value.replace(/ /g, '.');
                            var events = row.find(value);
                            var firstEvent = row.find(value).first();
                            var eventLength = row.find(value).length;
                            events.slice(1).remove();
                            firstEvent.addClass('days-' + eventLength);
                            firstEvent.addClass('slot-' + (k + 1));
                            firstEvent.show();
                        });
                    }
                });
            }
            if (calendarType === 'week') {
                jQuery('.single-event').remove();
                jQuery('.list-headline').remove();
                jQuery('.week-headline').remove();
                eventCalendar.removeClass('is-list');
                eventCalendar.removeClass('is-month');
                eventCalendar.addClass('is-week');
                eventCalendar.find('.my-calendar-nav .my-calendar-prev').after('<li class="current-week"><a href="' + window.location.origin + window.location.pathname + '?cid=my-calendar&time=week&dy=' + (date.getDate() - date.getDay() + 1) + '&month=' + date.toJSON().slice(5, 7) + '&yr=' + date.toJSON().slice(0, 4) + '">aktuelle Woche</a></li>');
                var eventsRow = eventCalendar.find('tbody .mc-row');
                var events = [];
                eventsRow.find('.calendar-event').map(function () {
                    return jQuery(this).attr('class');
                }).each(function (i, str) {
                    if (!~$.inArray(str, events)) {
                        events.push(str);
                    }
                });
                var params = {};
                params.action = 'get_listing_calendar';
                if (vars) {
                    if (!vars.yr) {
                        params.vars = '{"day":' + vars.dy + ',"month":' + vars.month + '}';
                    } else {
                        params.vars = '{"day":' + vars.dy + ',"month":' + vars.month + ',"year":' + vars.yr + '}';
                    }
                }
                jQuery.ajax(
                    '/wp-admin/admin-ajax.php',
                    {
                        method: 'GET',
                        data: params,
                        success: addEventList
                    }
                );
            }
            var list = jQuery('<a>', { class: 'button list-button', text: 'Liste' });
            list.click(getCalendarList);
            var existingListButtonItem = eventCalendar.find('.list-button');
            if (existingListButtonItem.length > 0) {
                existingListButtonItem.remove()
            }
            eventCalendar.find('.mc-time').append(list);
        }
    }
    function addEventList(result) {
        result = JSON.parse(result);
        var append = '';
        if (result.length > 0) {
            append += '<div class="headline-section week-headline"><div class="headline-section__content text-center container"><h2 class="headline-section__headline is-highlighted">Events der Woche</h2></div></div>';
        }
        jQuery.each(result, function (k, value) {
            append += '<div class="section single-event">';
            append += '<div class="card v-card" data-url="' + value.event_link + '">';
            if (value.event_image.length > 0) {
                append += '<div class="card__image-wrapper">';
                append += '<figure class="figure">';
                append += '<div class="figure__image-wrapper is-highlighted">';
                append += '<div class="figure__image">';
                append += '<img src="' + value.event_image + '" alt="">';
                append += '</div>';
                append += '</div>';
                append += '</figure>';
                append += '</div>';
            }
            append += '<div class="card__content">';
            append += '<ul class="card__meta-data no-list-style">';
            if (value.event_city.length > 0) {
                append += '<li class="card__meta-data-item is-date"><span class="locality">' + value.event_city + '</span></li>';
            }
            if (value.event_begin.length > 0) {
                append += '<li class="card__meta-data-item is-date">' + value.event_begin.slice(8, 10) + '.' + value.event_begin.slice(5, 7) + '.' + value.event_begin.slice(0, 4) + '</li>';
            }
            append += '</ul>';
            append += '<div class="card__main">';
            append += '<h3 class="card__headline">';
            if (value.event_title.length > 0) {
                append += '<span>' + value.event_title + '</span>';
            }
            append += '</h3>';
            append += '</div>';
            if (value.event_short.length > 0) {
                append += '<div class="event-teaser">';
                append += '<p>'+value.event_short+'</p>';
                append += '</div>';
            }
            append += '<div class="card__footer">';
            append += '<a href="' + value.event_link + '" ref="cta" class="card__cta is-link e-link" target="_self">';
            append += 'Mehr lesen';
            append += '<i class="card__icon fas fa-chevron-right" aria-hidden="true"></i>';
            append += '</a>';
            append += '</div>';
            append += '</div>';
            append += '</div>';
            append += '</div>';
        });
        jQuery('.mc-content').after(append);
    }
    var eventCalendar = jQuery('.mc-main');
    if (eventCalendar.length > 0) {
        if (window.location.href.indexOf('=week') > 0) {
            updateCalendar('week');
        } else if (window.location.href.indexOf('mc_id=') <= 0) {
            updateCalendar('month');
        } else if (window.location.href.indexOf('mc_id=') > 0) {
            jQuery('.breadcrumbs__item.is-last').click(function () {
                window.location = window.location.origin + window.location.pathname;
            });
        }
        jQuery(document).ajaxComplete(function (e, xhr, settings) {
            if (settings.url.indexOf('wp-admin') > 0) {
                return;
            }
            if (settings.url.indexOf('=week') > 0) {
                updateCalendar('week', getUrlVars(settings.url));
            } else {
                updateCalendar('month');
            }
        });
    }
    function getUrlVars(input) {
        var vars = [], hash;
        var hashes = input.slice(input.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    function getCalendarList() {
        var eventCalendar = jQuery('.mc-main');
        var date = new Date();
        jQuery('.my-calendar-table').empty();
        jQuery('.my-calendar-table').text('Lade...');
        eventCalendar.removeClass('is-week');
        eventCalendar.removeClass('is-month');
        eventCalendar.addClass('is-list');
        jQuery('.single-event').remove();
        jQuery('.week-headline').remove();
        jQuery('.mc-time').empty();
        jQuery('.my-calendar-nav').remove();
        jQuery('.mc-time').hide();
        jQuery('.mc-time').append('<a href="' + window.location.origin + window.location.pathname + '?cid=my-calendar&amp;time=month&amp;dy=' + (date.getDate() - date.getDay() + 1) + '&amp;month=' + date.toJSON().slice(5, 7) + '&amp;yr=' + date.toJSON().slice(0, 4) + '" class="month mcajax">Monat</a>');
        jQuery('.mc-time').append('<a href="' + window.location.origin + window.location.pathname + '?cid=my-calendar&amp;time=week&amp;dy=' + (date.getDate() - date.getDay() + 1) + '&amp;month=' + date.toJSON().slice(5, 7) + '&amp;yr=' + date.toJSON().slice(0, 4) + '" class="month mcajax">Woche</a>');
        if (jQuery('.mc-time .mc-active.week').length <= 0) {
            jQuery('.mc-time').append('<span class="mc-active week">Liste</span>');
        }
        jQuery.ajax(
            '/wp-admin/admin-ajax.php',
            {
                method: 'GET',
                data: { action: 'get_all_events' },
                success: displayFullList
            }
        );
    }
    function displayFullList(result) {
        result = JSON.parse(result);
        jQuery('.my-calendar-table').text('');
        jQuery('.mc-time').show();
        var append = '';
        jQuery.each(result, function (k, innerValue) {
            if (innerValue.length > 0) {
                if (k === 'future') {
                    append += '<div class="headline-section list-headline"><div class="headline-section__content text-center container"><h2 class="headline-section__headline is-highlighted">Bevorstehende Events</h2></div></div>';
                }
                if (k === 'past') {
                    append += '<div class="headline-section list-headline"><div class="headline-section__content text-center container"><h2 class="headline-section__headline is-highlighted">Vergangene Events</h2></div></div>';
                }
            }
            jQuery.each(innerValue, function (k, value) {
                append += '<div class="section single-event">';
                append += '<div class="card v-card" data-url="' + value.event_link + '">';
                if (value.event_image.length > 0) {
                    append += '<div class="card__image-wrapper">';
                    append += '<figure class="figure">';
                    append += '<div class="figure__image-wrapper is-highlighted">';
                    append += '<div class="figure__image">';
                    append += '<img src="' + value.event_image + '" alt="">';
                    append += '</div>';
                    append += '</div>';
                    append += '</figure>';
                    append += '</div>';
                }
                append += '<div class="card__content">';
                append += '<ul class="card__meta-data no-list-style">';
                if (value.event_city.length > 0) {
                    append += '<li class="card__meta-data-item is-date"><span class="locality">' + value.event_city + '</span></li>';
                }
                if (value.event_begin.length > 0) {
                    append += '<li class="card__meta-data-item is-date">' + value.event_begin.slice(8, 10) + '.' + value.event_begin.slice(5, 7) + '.' + value.event_begin.slice(0, 4) + '</li>';
                }
                append += '</ul>';
                append += '<div class="card__main">';
                append += '<h3 class="card__headline">';
                if (value.event_title.length > 0) {
                    append += '<span>' + value.event_title + '</span>';
                }
                append += '</h3>';
                append += '</div>';
                if (value.event_short.length > 0) {
                    append += '<div class="event-teaser">';
                    append += '<p>'+value.event_short+'</p>';
                    append += '</div>';
                }
                append += '<div class="card__footer">';
                append += '<a href="' + value.event_link + '" ref="cta" class="card__cta is-link e-link" target="_self">';
                append += 'Mehr lesen';
                append += '<i class="card__icon fas fa-chevron-right" aria-hidden="true"></i>';
                append += '</a>';
                append += '</div>';
                append += '</div>';
                append += '</div>';
                append += '</div>';
            });
        });
        jQuery('.mc-content').after(append);
    }

    /** Card carousel */
    var carouselCards = jQuery('.page__cards');

    if (carouselCards.children().length > 0) {
        carouselCards.attr('id', 'card_carousel_indicators')
        carouselCards.addClass('carousel slide')
        carouselCards.attr('data-ride', 'carousel')

        var childs = carouselCards.children();

        carouselCards.prepend('<div class="carousel-inner"></div>');

        $('.carousel-inner').prepend(childs);
        var carouselInner = $('.carousel-inner');

        for (var i = 0; i <= carouselInner.children().length; i++) {
            if (i == 0) {
                carouselInner.children().eq(i).addClass('active')
            }

            carouselInner.children().eq(i).addClass('carousel-item')
        }

        carouselInner.after('<a class="carousel-control-prev" href="#card_carousel_indicators" role="button" data-slide="prev">\n' +
            '    <span class="carousel-control-prev-icon" aria-hidden="true"></span>\n' +
            '    <span class="sr-only">Previous</span>\n' +
            '  </a>\n' +
            '  <a class="carousel-control-next" href="#card_carousel_indicators" role="button" data-slide="next">\n' +
            '    <span class="carousel-control-next-icon" aria-hidden="true"></span>\n' +
            '    <span class="sr-only">Next</span>\n' +
            '  </a>')
    }

    var eventCardss = jQuery('.event__cards');

    if ($('.event__cards .section__content ').children().length > 0) {
        var eventCardChildren = $('.event__cards .section__content ').children();

        var eventCardDate = eventCardChildren.eq(0).find('.card__content').find('.card__meta-data');


        eventCardChildren.eq(0).find('.rte-content').remove()

        //eventCardChildren.find('.card__content').prepend(eventCardDate);

        for (var i = 0; i <= eventCardChildren.length; i++) {
            var eventCardDate = eventCardChildren.eq(i).find('.card__content').find('.card__meta-data');

            eventCardChildren.eq(i).find('.rte-content').remove()

            eventCardChildren.eq(i).find('.card__content').prepend(eventCardDate);
        }
    }
    var sectionHeightImages = jQuery('.section.do-height-calc');
    if (sectionHeightImages.length > 0) {
        let orientation = sectionHeightImages.data('card-image-orientation')
        let targetHeight = sectionHeightImages.data('card-image-height')
        sectionHeightImages.find('.card.is-vertical').each(function (k, value) {
            value = jQuery(value);
            var cardImg = value.find('.card__image-wrapper .figure__image img');
            if (cardImg.length > 0) {
                cardImg.height(targetHeight);
                let cssOrientation = 'center';
                switch (orientation) {
                    case 'image-top-left':
                        cssOrientation = 'top left';
                        break;
                    case 'image-top-center':
                        cssOrientation = 'top center';
                        break;
                    case 'image-top-right':
                        cssOrientation = 'top right';
                        break;
                    case 'image-middle-left':
                        cssOrientation = 'center left';
                        break;
                    case 'image-middle-center':
                        cssOrientation = 'center';
                        break;
                    case 'image-middle-right':
                        cssOrientation = 'center right';
                        break;
                    case 'image-bottom-left':
                        cssOrientation = 'bottom left';
                        break;
                    case 'image-bottom-center':
                        cssOrientation = 'bottom center';
                        break;
                    case 'image-bottom-right':
                        cssOrientation = 'bottom right';
                        break;
                    default:
                        cssOrientation = 'center';
                        break;
                }
                cardImg.css('object-position', cssOrientation)
            }
            value.addClass('height-calc-done')
        })
    }

    /**
     * Card Carousel Slider
     */
    var wjdCardSwiper = jQuery('.wjd-card-swiper');
    if (wjdCardSwiper.length > 0) {
        wjdCardSwiper.each(function (k, value) {
            value = jQuery(value);
            value.closest('.container').addClass('slider-fullwidth');
            if (value.find('.swiper-slide').length > 1) {
                new Swiper('.swiper', {
                    direction: 'horizontal',
                    effect: 'slide',
                    freeModeSticky: true,
                    slidesPerView: 1,
                    loop: true,
                    loopAdditionalSlides: 1,
                    centeredSlides: true,
                    freeMode: false,
                    spaceBetween: 48,
                    autoHeight: true,
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true,
                        renderBullet: function (index, className) {
                            return '<li class="pagination-item ' + className + '"></li>';
                        }
                    }
                });
            }
        })
    }

    /**
     * Anchormenu
     */
    var anchorMenu = jQuery('.anchormenu');
    if (anchorMenu.length > 0) {
        var headlineSections = jQuery('.headline-section');
        anchorMenu.find('.single-anchor').each(function (k, value) {
            value = jQuery(value);
            value.click(function () {
                jQuery('body, html').animate({
                    scrollTop: jQuery(headlineSections[value.data('key')]).offset().top
                }, 400)
            })
        })
    }
    var anchorMenuSticky = jQuery('.anchormenu-sticky');
    var sectionHeadlines = jQuery('div.headline-section h2.headline-section__headline');
    sectionHeadlines = sectionHeadlines.filter(function(i, elem) {
        return elem.innerText.trim().length > 0;
    })
    var anchorCollection = [];
    if (anchorMenuSticky.length > 0 && sectionHeadlines.length > 0) {
        sectionHeadlines.each(function(k, value) {
            value = jQuery(value);
            var anchor = jQuery('<p class="single-anchor" data-key="'+k+'">'+value.text()+'</p>');
            anchor.click(function () {
                if (anchorMenuSticky.hasClass('scrolling')) {
                    return;
                }
                anchorMenuSticky.addClass('scrolling')
                jQuery('body, html').animate({
                    scrollTop: jQuery(sectionHeadlines[anchor.data('key')]).offset().top
                }, 400);
                setTimeout(function() {                        
                    anchorMenuSticky.removeClass('scrolling')
                }, 400);
                if (jQuery(window).width() < 992) {
                    anchorMenuSticky.removeClass('open');                        
                }
            })
            anchorMenuSticky.find('.anchor-menu-content').append(anchor);
            anchorCollection.push(anchor);
        })
        anchorMenuSticky.show(); 

        var prevScroll = null;
        var nextScroll = null;
        var currentIndex = 0;
        var offsetValue = 100;

        nextScroll = jQuery(sectionHeadlines.get(currentIndex)).offset().top - offsetValue;
        
        jQuery(document).scroll(function(event) {
            var scrollTop = jQuery(window).scrollTop();

            if (nextScroll !== null && scrollTop > nextScroll) {
                while (sectionHeadlines.get(currentIndex) && jQuery(sectionHeadlines.get(currentIndex)).offset().top < scrollTop) {
                    currentIndex++;
                }
                setActiveAnchor();
            } else if (prevScroll !== null && currentIndex > 0 && scrollTop < prevScroll) {
                while (sectionHeadlines.get(currentIndex) && jQuery(sectionHeadlines.get(currentIndex)).offset().top > scrollTop) {
                    currentIndex--;
                }
                setActiveAnchor();
            }
        });

        function setActiveAnchor() {
            if (currentIndex >= sectionHeadlines.length) {
                currentIndex = sectionHeadlines.length-1;
            } else if (currentIndex < 0) {
                currentIndex = 0;
            }

            if (currentIndex < sectionHeadlines.length-1) {
                nextScroll = jQuery(sectionHeadlines.get(currentIndex+1)).offset().top - offsetValue;
            } else {
                nextScroll = null;
            }
    
            if (currentIndex >= 0) {
                prevScroll = jQuery(sectionHeadlines.get(currentIndex)).offset().top - offsetValue;
            } else {
                prevScroll = null;    
            }

            anchorMenuSticky.find('.anchor-menu-content').find('.active').removeClass('active');
            anchorCollection[currentIndex].addClass('active');
            var text = anchorCollection[currentIndex].text();
            if (text.length > 10) {
                text = text.substring(0, 10) + '...';
            }
            anchorMenuSticky.find('.anchor-menu-button p span.text').text(text)
        }

        anchorMenuSticky.find('.anchor-menu-button').on('click', function() {
            anchorMenuSticky.addClass('open');
        });
        anchorMenuSticky.find('.anchor-menu-close').on('click', function() {
            anchorMenuSticky.removeClass('open');
        });
        jQuery(document).on('click', function(e) {
            var target = jQuery(e.target);
            if (target.closest('.anchormenu-sticky').length <= 0) {
                anchorMenuSticky.removeClass('open');
            }
        })

        jQuery(document).scroll();
    }

    /*
     * Header Search Form
     */
    var searchForm = jQuery('header form.search-nav');
    if (searchForm.length > 0) {
        var searchToggle = searchForm.find('.search-toggle');
        var searchInput = searchForm.find('.search-field');
        var logo = jQuery('.custom-logo');
        searchToggle.click(function () {
            var maxWidth = searchForm.offset().left - (logo.offset().left + logo.width()) - 30;
            if (!searchForm.hasClass('active')) {
                searchForm.addClass('active');
                searchInput.outerWidth(maxWidth);
            }
        })
        searchForm.find('.search-icon').click(function (e) {
            if (searchForm.hasClass('active')) {
                e.stopPropagation();
                searchForm.removeClass('active');
                searchInput.outerWidth(0);
            }
        })
        searchForm.find('.search-text').click(function () {
            if (searchForm.hasClass('active')) {
                searchForm.submit();
            }
        })
    }

    /**
     * Post Slider
     */
    var wjdPostSwiper = jQuery('.wjd-post-swiper');
    if (wjdPostSwiper.length > 0) {
        wjdPostSwiper.each(function (key, value) {
            value = jQuery(value);
            if (value.find('.swiper-slide').length > 1) {
                new Swiper('.swiper', {
                    direction: 'horizontal',
                    effect: 'slide',
                    freeModeSticky: true,
                    loop: true,
                    loopAdditionalSlides: 1,
                    centeredSlides: false,
                    freeMode: false,
                    breakpoints: {
                        320: {
                            slidesPerView: 1,
                            spaceBetween: 20,
                            autoHeight: true,
                            navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev',
                                enabled: true
                            },
                        },
                        600: {
                            slidesPerView: 2,
                            spaceBetween: 20,
                            autoHeight: true,
                            navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev',
                                enabled: true
                            },
                        },
                        992: {
                            slidesPerView: value.attr('data-visible-items'),
                            spaceBetween: 48,
                            autoHeight: false,
                            navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev',
                                enabled: true
                            },
                        }
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true,
                        renderBullet: function (index, className) {
                            return '<li class="pagination-item ' + className + '"></li>';
                        }
                    }
                });
            }
        });
    }

    var wjdPostMasonry = jQuery('.wjd-post-masonry');
    if (wjdPostMasonry.length > 0) {
        wjdPostMasonry.each(function (key, value) {
            value = jQuery(value);
            value.imagesLoaded(function () {
                value.masonry({
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-sizer',
                    percentPosition: true,
                    gutter: 20
                });
                value.css('opacity', 1);
            });
        });
    }

    var wjdPostArchive = jQuery('.wjd-post-archive.ajax-archive, .wjd-post-masonry.ajax-archive');
    if (wjdPostArchive.length > 0) {
        wjdPostArchive.each(function (key, value) {
            value = jQuery(value);
            var loadMore = value.find('.load-more');
            loadMore.on('click', function (e) {
                e.preventDefault();
                if (!loadMore.hasClass('loading')) {
                    loadMore.addClass('loading');
                    var data = {};
                    var posttypeToLoad = 'posts';
                    if (loadMore.attr('data-posttype')) {
                        posttypeToLoad = loadMore.attr('data-posttype');
                    }
                    data.action = 'wjd_load_'+posttypeToLoad;
                    data.loaded = Number(value.attr('data-loaded'));
                    data.toLoad = Number(value.attr('data-items-to-load'));
                    if (value.attr('data-category')) {
                        data.category = Number(value.attr('data-category'));
                    }
                    data.perRow = Number(value.attr('data-per-row'));
                    data.type = value.attr('data-type');
                    data.offset = value.attr('data-offset');
                    jQuery.ajax(
                        '/wp-admin/admin-ajax.php',
                        {
                            method: 'POST',
                            data: data,
                            success: function (res) {
                                var res = JSON.parse(res);
                                if (value.attr('data-type') === 'grid') {
                                    var spaceToFill = data.perRow - (data.loaded % data.perRow);
                                    var lastRow = value.find('.row').last();
                                    if (spaceToFill > 0) {
                                        for (var i = 0; i < spaceToFill; i++) {
                                            lastRow.append(res[i]);
                                        }
                                        res.splice(0, i);
                                    }
                                    if (res.length > 0) {
                                        lastRow.after(jQuery('<div>', { class: 'row' }));
                                    }
                                    for (var ri = 0; ri < res.length; ri++) {
                                        var lastRow = value.find('.row').last();
                                        lastRow.append(res[ri]);
                                    }
                                } else if (value.attr('data-type') === 'masonry') {
                                    var newRes = [];
                                    for (var index = 0; index < res.length; index++) {
                                        newRes.push(htmlToElement(res[index]));
                                    }
                                    var $res = jQuery(newRes);
                                    value.find('.grid-item').last().after($res);
                                    value.imagesLoaded(function () {
                                        value.masonry('appended', $res);
                                    });
                                } else if (value.attr('data-type') === 'list') {
                                    value.find('.views-row').last().after(res);
                                }

                                value.attr('data-loaded', data.loaded + data.toLoad);
                                loadMore.removeClass('loading');
                                if (data.loaded + data.toLoad >= value.attr('data-max')) {
                                    loadMore.hide();
                                }
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        }
                    );
                }
            })
        });
    }

    /**
     * Dynamic Datatables
     */

    var datatableHolder = jQuery('.datatable-holder');
    if (datatableHolder.length > 0) {
        var src = datatableHolder.attr('data-wjdsrc');
        var colums = datatableHolder.attr('data-wjdcolumns');
        if (colums.charAt(0) === '{') {
            colums = colums.slice(1);
        }
        if (colums.charAt(colums.length - 1) === '}') {
            colums = colums.slice(0, -1);
        }
        var columnSplit = colums.split('}{');
        var columnsArray = [];
        columnSplit.forEach(part => {
            columnsArray.push({
                title: part.split(',')[0],
                data: part.split(',')[1],
                defaultContent: ''
            })
        });
        if (columnsArray.length > 0) {
            jQuery.ajax(src, {
                success: function (data) {
                    if (typeof data === 'object') {
                        data = data.data;
                    }
                    datatableHolder.DataTable({
                        processing: true,
                        responsive: true,
                        data: data,
                        columns: columnsArray,
                        language: {
                            paginate: {
                                previous: "&lt;",
                                next: "&gt;"
                            },
                            search: "Suchen",
                            info: "Zeige _START_ bis _END_ von _TOTAL_ Einträgen",
                            infoEmpty: "",
                            lengthMenu: "Zeige _MENU_ Einträge",
                            infoFiltered: "(gefiltert von _MAX_ Einträgen)",
                            emptyTable: "Keine Daten gefunden",
                            zeroRecords: "Keine passenden Einträge gefunden"
                        },
                        fnDrawCallback: function(oSettings) {
                            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                                jQuery(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                            } else {
                                jQuery(oSettings.nTableWrapper).find('.dataTables_paginate').show();
                            }
                        }
                    });
                }
            })
        }
    }
});

var time = 0;

window.addEventListener('scroll', function () {
    var $toTop = $('#toTop');

    $toTop.toggleClass('is-visible', (window.scrollY || document.body.scrollTop) > 0);
});

function smoothScrolltoTop() {
    var $body = $('html, body');
    $body.stop().animate({ scrollTop: 0 }, 500);
}

function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim();
    template.innerHTML = html;
    return template.content.firstChild;
}

/**
 * Wrap Card images with Links
 */
$(window).on('load', function () {
    var $card = $('.card');

    $card.each(function (index, item) {
        var $link = $(item).find('a.card__cta.is-link');
        var $image = $(item).find('.figure__image img');

        if ($link.length) {
            $image.wrap('<a class="card__image-link" href="' + $link.attr('href') + '"></a>');
        }
    });
});

